<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class BinnacleFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'Binnacle';
    }
}