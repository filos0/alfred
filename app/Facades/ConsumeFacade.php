<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ConsumeFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'ConsumeInterface';
    }
}