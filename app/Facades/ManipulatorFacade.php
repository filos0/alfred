<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class ManipulatorFacade extends Facade
{
    protected static function getFacadeAccessor() {
        return 'Manipulator';
    }
}