<?php

namespace App\Http\Controllers;

use Request;

use Illuminate\Support\Facades\Input;
use Illuminate\Routing\Controller as BaseController;


class API_Controller extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function lista_curp()
    {
        if (Request::ajax()) {

            $curp = Input::get('curp');
            $curp = $curp . "%";

            $lista_curp = \DB::connection('oracle')->select("
            SELECT * FROM conductores 
            WHERE  curp LIKE '" . $curp . "'");
            $lista_curp = array_map(function ($item) {
                return (array)$item;
            }, $lista_curp);

            return $lista_curp;
        }
    }

    public function lista_nombre()
    {
        if (Request::ajax()) {

            $nombre = Input::get('nombre');
            $primer_apellido = Input::get('primer_apellido');
            $segundo_apellido = Input::get('segundo_apellido');

            $nombre = $nombre . "%";
            $primer_apellido = $primer_apellido . "%";
            $segundo_apellido = $segundo_apellido . "%";

            $lista_curp = \DB::connection('oracle')->select("
            SELECT * FROM conductores 
            WHERE  paterno 
            LIKE '" . $primer_apellido . "' AND materno LIKE '" . $segundo_apellido . "' AND nombre LIKE '" . $nombre . "'
            ");
            $lista_curp = array_map(function ($item) {
                return (array)$item;
            }, $lista_curp);

            return $lista_curp;
        }
    }

}
