<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class ManageController extends Controller
{
    public function GetUsers()
    {
        $users = User::all();

        $protectedUsers = array();
        foreach ($users as $user) {
            $protectedUser = new \stdClass();

            $protectedUser->id = encrypt($user->id);
            $protectedUser->name = $user->name;
            $protectedUser->first_surname = $user->ap_paterno;
            $protectedUser->second_surname = $user->ap_materno;
            $protectedUser->status = $user->estatus_id;
            $protectedUser->module = $user->id_modulo;
            $protectedUser->username = $user->username;
            $protectedUser->belongs = $user->pertenece;

            $protectedUsers[] = $protectedUser;
        }

        $users = $protectedUsers;

        return view('auth.manage')->with('users', $users);
    }

    public function ShowUser($id)
    {
        $db = DB::connection('auth');

        try {
            $user = User::findOrFail(decrypt($id));
        } catch (DecryptException $e) {
            return view('errors.1001');
        }

        $modules = $db->table('modulos')
            ->select('modulos.NumeroModulo', 'modulos.NombreModulo')
            ->get();

        $roles = $db->table('roles')
            ->select('roles.id', 'roles.description', 'roles.name')
            ->get();

        if (empty($user)) {
            return view('errors.1001');
        }

        $protectedUser = new \stdClass();

        $protectedUser->id = $user->id;
        $protectedUser->name = $user->name;
        $protectedUser->first_surname = $user->ap_paterno;
        $protectedUser->second_surname = $user->ap_materno;
        $protectedUser->status = $user->estatus_id;
        $protectedUser->module = $user->id_modulo;
        $protectedUser->username = $user->username;
        $protectedUser->belongs = $user->pertenece;

        return view('auth.edit')->with([
            'user' => $user,
            'modules' => $modules,
            'roles' => $roles
        ]);
    }

    public function UpdateUser(Request $request)
    {
        $keys = array_keys($request->all());

        $db = DB::connection('auth');

        try {
            $id = decrypt($request->input('id'));
        } catch (DecryptException $e) {
            return view('errors.1001');
        }

        foreach ($keys as $key) {
            switch ($key) {
                case 'name':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $user = User::findOrFail($id);

                    $user->name = $request->input($key);

                    $user->save();

                    \Binnacle::save('Update name', $id . '__' . $request->input($key));

                    break;

                case 'first_surname':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $user = User::findOrFail($id);

                    $user->ap_paterno = $request->input($key);

                    $user->save();

                    \Binnacle::save('Update first surname', $id . '__' . $request->input($key));

                    break;

                case 'second_surname':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $user = User::findOrFail($id);

                    $user->ap_materno = $request->input($key);

                    $user->save();

                    \Binnacle::save('Update second surname', $id . '__' . $request->input($key));

                    break;

                case 'username':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $validate = Validator::make($request->all(), [
                        'username' => 'unique:users'
                    ]);

                    if ($validate->fails()) {
                        session()->flash('error', 'Ese username ya esta ocupado\nElige otro por favor');

                        \Binnacle::save('Error when updating username', $id . '__' . $request->input($key));

                        return redirect(route('users_show', ['id' => $request->input('id')]));
                    }

                    $user = User::findOrFail($id);

                    $user->username = $request->input($key);

                    $user->save();

                    \Binnacle::save('Update username', $id . '__' . $request->input($key));

                    break;

                case 'module':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $user = User::findOrFail($id);

                    $user->id_modulo = $request->input($key);

                    $user->save();

                    \Binnacle::save('Update module', $id . '__' . $request->input($key));

                    break;

                case 'belongs':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $user = User::findOrFail($id);

                    $user->pertenece = $request->input($key);

                    $user->save();

                    \Binnacle::save('Update area', $id . '__' . $request->input($key));

                    break;

                case 'password':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $password = $request->input('password');

                    $check_password = $request->input('check_password');

                    if ($password != $check_password) {
                        session()->flash('error', 'Las contraseñas no coinciden\nVerifique que coincidan e intentelo de nuevo');

                        \Binnacle::save('Error when updating password', $id . '__' . $request->input($key));

                        return redirect(route('users_show', ['id' => $request->input('id')]));
                    }

                    $user = User::findOrFail($id);

                    $user->password = Hash::make($password);

                    $user->save();

                    \Binnacle::save('Update password', $id . '__' . $request->input($key));

                    break;

                case 'role':
                    if (empty($request->input($key))) {
                        break;
                    }

                    $roles = $db->table('roles')
                        ->select('roles.id', 'roles.description', 'roles.name')
                        ->get();

                    $newRoles = $request->input($key);

                    $db->table('role_user')->where('user_id', '=', $id)->delete();

                    foreach ($newRoles as $new) {
                        foreach ($roles as $role) {
                            if ($new == $role->id) {

                                \Binnacle::save('Update role', $id . '__' . $role->id);

                                $db->table('role_user')->insert([
                                    'user_id' => $id,
                                    'role_id' => $role->id
                                ]);
                            }
                        }
                    }

                    break;
            }
        }

        if (!isset($request->role)) {
            $db->table('role_user')->where('user_id', '=', $id)->delete();

            \Binnacle::save('Delete all roles', $id);
        }

        session()->flash('success', 'Se han guardado los cambios');

        return redirect(route('users_manage'));
    }

    public function DisableUser($id)
    {
        try {
            $id_d = decrypt($id);

            $user = User::findOrFail($id_d);
        } catch (DecryptException $e) {
            return view('errors.1001');
        }

        $user->estatus_id = 'B';

        $user->save();

        session()->flash('success', 'Se han guardado los cambios');

        \Binnacle::save('Disable User', $id_d);

        return redirect(route('users_manage'));
    }

    public function EnableUser($id)
    {
        try {
            $id_d = decrypt($id);

            $user = User::findOrFail($id_d);
        } catch (DecryptException $e) {
            return view('errors.1001');
        }

        $user->estatus_id = 'A';

        $user->save();

        session()->flash('success', 'Se han guardado los cambios');

        \Binnacle::save('Enable User', $id_d);

        return redirect(route('users_manage'));
    }
}
