<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class RegisterController extends Controller
{
    public function CreateUser()
    {
        $db = DB::connection('auth');

        $modules = $db->table('modulos')
            ->select('modulos.NumeroModulo', 'modulos.NombreModulo')
            ->get();

        $roles = $db->table('roles')
            ->select('roles.id', 'roles.description', 'roles.name')
            ->get();

        return view('auth.register')->with([
            'modules' => $modules,
            'roles' => $roles
        ]);
    }

    public function InsertUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'unique:users',
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Ese username ya esta ocupado\nVerifique si el usuario que esta intentando crear ya ha sido creado');

            return redirect(route('users_create'));
        }

        if ($request->input('password') != $request->input('check_password')) {
            session()->flash('error', 'Las contraseñas no coinciden\nVerifique e intentelo de nuevo');

            return redirect(route('users_create'));
        }

        $user = new User();

        $user->name = $request->input('name');
        $user->ap_paterno = $request->input('first_surname');

        if (empty($request->input('second_surname'))) {
            $user->ap_materno = 'X';
        } else {
            $user->ap_materno = $request->input('second_surname');
        }

        $user->username = $request->input('username');

        $user->email = $request->input('username');

        $user->password = Hash::make($request->input('check_password'));

        $user->estatus_id = 'A';

        $user->id_modulo = $request->input('module');

        $user->pertenece = $request->input('belongs');

        $user->save();

        $id = $user->id;

        \Binnacle::save('Create user', $id);

        if (isset($request->role)) {
            $db = DB::connection('auth');

            $roles = $db->table('roles')
                ->select('roles.id', 'roles.description', 'roles.name')
                ->get();

            $newRoles = $request->role;

            foreach ($newRoles as $new) {
                foreach ($roles as $role) {
                    if ($new == $role->id) {

                        \Binnacle::save('Assign role', $id . '__' . $role->id);

                        $db->table('role_user')->insert([
                            'user_id' => $id,
                            'role_id' => $role->id
                        ]);
                    }
                }
            }
        }

        session()->flash('success', 'Se ha creado el usuario');

        return redirect(route('users_manage'));
    }
}
