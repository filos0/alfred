<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class ResetPasswordController extends Controller
{
    function Reset(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required',
            'check_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            session()->flash('error', 'Las contraseñas no coinciden\nVerifique e intentelo de nuevo');

            return redirect(url()->previous());
        }

        $user = Auth::user();

        $user->password = Hash::make($request->input('check_password'));

        \Binnacle::save('Reset Pasword', $user->getAuthIdentifier());

        Auth::logout();

        return redirect(route('home'));
    }
}
