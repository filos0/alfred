<?php

namespace App\Http\Controllers\AvailablePlate;

use App\Http\Controllers\Controller;

class ConsultController extends Controller
{
    public function GetAvailablePlates()
    {
        $body = array(
            'service' => 'plates'
        );

        $plates = \Consumer::consume('http://128.222.200.41:7777/alfred/vehicles/type/particular', json_encode($body));

        if ($plates[0]->errCode == 0) {

            foreach ($plates as $plate) {
                switch ($plate->module) {
                    case 'B. JUAREZ':
                        $plate->module = 'BENITO JUAREZ';
                        break;

                    case 'M. CONTRERAS':
                        $plate->module = 'MAGDALENA CONTRERAS';
                        break;

                    case 'A. OBREGON':
                        $plate->module = 'ALVARO OBREGON';
                        break;

                    case 'C. DE ABASTOS':
                        $plate->module = 'CENTRAL DE ABASTOS';
                        break;
                }
            }

            return view('AvailablePlates.results')->with('modules', $plates);
        } else {
            return view('errors.1001');
        }
    }
}
