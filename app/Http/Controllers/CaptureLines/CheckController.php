<?php

namespace App\Http\Controllers\CaptureLines;

use App\Http\Controllers\Controller;
use App\Services\LicenseService;
use App\Services\VehicleService;
use Illuminate\Http\Request;

class CheckController extends Controller
{
    function check(Request $request, VehicleService $vehicle, LicenseService $license)
    {
        $capture_line = strtoupper($request->input('capture_line'));
        session()->flash('capture', $capture_line);

        if (strlen($capture_line) != 20) {
            session()->flash('error_', '¡La linea de captura no tiene la longitud adecuada!');
            return redirect(route('line_search'));
        }

        $body = [
            'linea' => $capture_line
        ];

        $line = \Consumer::consume('http://128.222.200.178:8113/ConsultaLC', json_encode($body));

        if ($line[0]->errCode == 1) {
            session()->flash('error_', '¡La linea de captura aun no ha sido pagada o generada!');
            return redirect(route('line_search'));
        }

        session()->flash('concept', $line[0]->idConcepto);
        session()->flash('plate', $line[0]->placa);
        session()->flash('cost', $line[0]->total);
        session()->flash('pay_date', date('d-m-o', strtotime($line[0]->fechaCobro)));
        session()->flash('description', $line[0]->descripcionIdPago);

        $body = [
            'service' => 'capture_line',
            'procedure' => [
                'capture_line' => $capture_line,
                'version' => ''
            ]
        ];

        $line = $license->verifyCaptureLine('http://128.222.200.41:7777/alfred/licenses/type/', $body);

        if (!empty($line)) {
            session()->flash('success_', 'La linea de captura ya ha sido pagada y usada');
            session()->flash('procedure', 'Licencias');
            session()->flash('date_use', date('d-m-o', strtotime($line['date_of_use'])));
            session()->flash('use', strtoupper($line['type']));
            return redirect(route('line_search'));
        }

        $line = $vehicle->verifyCaptureLine('http://128.222.200.41:7777/alfred/vehicles/type/', $body);

        if (!empty($line)) {
            session()->flash('success_', 'La linea de captura ya ha sido pagada y usada');
            session()->flash('procedure', 'Control Vehicular');
            session()->flash('date_use', date('d-m-o', strtotime($line['date_of_use'])));
            session()->flash('use', $line['type']);
            session()->flash('use_version', $line['version']);
            return redirect(route('line_search'));
        }

        session()->flash('success_', 'La linea de captura ya ha sido pagada');
        return redirect(route('line_search'));
    }
}
