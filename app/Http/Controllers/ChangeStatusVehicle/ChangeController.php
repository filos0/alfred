<?php

namespace App\Http\Controllers\ChangeStatusVehicle;

use App\Http\Controllers\Controller;
use App\Services\REPUVEService;
use App\Services\VehicleService;
use Illuminate\Http\Request;

class ChangeController extends Controller
{
    function changeDown(Request $request, VehicleService $vehicleService, REPUVEService $repuve)
    {
        $plate = $request->input('plate');

        \Binnacle::save('Status Down', $plate);

        if (!$vehicleService->verifyTenures($plate)) {
            session()->flash('error', 'No se autorizó la baja\nEl vehiculo debe una o más tenencias');
            return redirect(route('change_status'));
        }

        $report = $repuve->verifyStoleReportByPlate($plate);

        if (explode(':', $report[0])[0] === 'ERR') {
            session()->flash('error', 'No se autorizó la baja\nEl vehiculo tiene reporte de robo');
            return redirect(route('change_status'));
        }

        $body = array(
            'service' => 'movement',
            'vehicle' => array(
                'plate' => $plate
            )
        );

        $movements = \Consumer::consume('http://128.222.200.41:7777/alfred/vehicles/type/particular', json_encode($body));

        foreach ($movements as $movement) {
            if ($movement->movement == 'BAJA') {
                if ($vehicleService->setDownStatus($plate)) {
                    session()->flash('success', 'El vehiculo se ha dado de baja');
                    return redirect(route('change_status'));
                }

                session()->flash('error', 'No se autorizó la baja\nHubo un problema, favor de comunicarse con sistemas');
                return redirect(route('change_status'));
            }
        }

        session()->flash('error', 'No se autorizó la baja\nEl vehiculo no tiene movimento de baja');
        return redirect(route('change_status'));
    }
}
