<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use FTP;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\PDF_Controller as PDFC;


class K8_Controller extends BaseController
{
    function __construct(PDFC $pdfc)
    {
        $this->PDFC = $pdfc;
        $this->middleware('auth');
    }

    public function inicio(Request $request)
    {
        return view('K8');
    }

    public function actualizar(Request $request)
    {

        if (file_exists($request->url_huella2)) {
            // Si existia la huella 2
            $fileFrom = 'licencias/' . $request->ultimo_folio . '2.wsq';
            $filename = $request->nuevo_folio . '2.wsq';
            $fileTo = "/Juanito/Huella2/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        } else {
            // Se cargan la huella 2 por defecto
            $fileFrom = 'licencias/huella2.wsq';
            $filename = $request->nuevo_folio . '2.wsq';
            $fileTo = "/Juanito/Huella2/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        }

        if (file_exists($request->url_huella2)) {
            // Si existia la huella 2
            $fileFrom = 'licencias/' . $request->ultimo_folio . '7.wsq';
            $filename = $request->nuevo_folio . '7.wsq';
            $fileTo = "/Juanito/Huella7/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);


        } else {
            // Se cargan la huella 2 por defecto
            $fileFrom = 'licencias/huella7.wsq';
            $filename = $request->nuevo_folio . '7.wsq';
            $fileTo = "/Juanito/Huella7/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        }

        if ($request->hasFile('otra_foto')) {
            /* Se almacena */
            $file_foto = $request->file('otra_foto');
            $destinationPath = "licencias_nuevas/";
            $filename = $request->nuevo_folio . '.jpg';
            $file_foto->move($destinationPath, $filename);

            /* Se envia con la ñora*/
            $fileFrom = 'licencias_nuevas/' . $request->nuevo_folio . '.jpg';
            $fileTo = "/Juanito/foto/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        } else {

            $fileFrom = 'licencias/' . $request->ultimo_folio . '.jpg';
            $filename = $request->nuevo_folio . '.jpg';
            $fileTo = "/Juanito/foto/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        }
        if ($request->hasFile('otra_firma')) {
            /* Se almacena */
            $file_firma = $request->file('otra_firma');
            $destinationPath = "licencias_nuevas/";
            $filename = $request->nuevo_folio . '.bmp';
            $file_firma->move($destinationPath, $filename);

            /* Se envia con la ñora*/
            $fileFrom = 'licencias_nuevas/' . $request->nuevo_folio . '.bmp';
            $fileTo = "/Juanito/Firma/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        } else {

            $fileFrom = 'licencias/' . $request->ultimo_folio . '.bmp';
            $filename = $request->nuevo_folio . '.bmp';
            $fileTo = "/Juanito/Firma/" . $filename;
            FTP::connection('FTP_ñora')->uploadFile($fileFrom, $fileTo);

        }

        return view('Finalizar');
    }

    public function get_Fotos(Request $request)
    {

        if ($request->folio) {
            $lugar_fotos = \DB::connection('oracle')->select("
                 SELECT to_char(fecha_alta,'WW') semana,to_char(fecha_alta,'YYYY') anio 
                FROM LICENCIAS 
                 WHERE FOLIO ='" . $request->folio . "'
            ");
            $lugar_fotos = array_map(function ($item) {
                return (array)$item;
            }, $lugar_fotos);

            $año = $lugar_fotos[0]['anio'];
            $semana = $lugar_fotos[0]['semana'];
            $folio = $request->folio;

            //dd($año.$semana.$folio);

            $fileFrom = "/" . $año . "/Semana" . $semana . "/Foto/" . $folio . ".jpg";
            $fileTo_foto = "licencias/" . $folio . ".jpg";
            $foto = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_foto);

            if ($foto == false) {
                $semana = $semana + 1;
                $fileFrom = "/" . $año . "/Semana" . $semana . "/Foto/" . $folio . ".jpg";
                $foto = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_foto);
            }

            $fileFrom = "/" . $año . "/Semana" . $semana . "/Firma/" . $folio . ".bmp";
            $fileTo_firma = "licencias/" . $folio . ".bmp";
            $firma = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_firma);
            if ($firma == false) {
                $semana = $semana + 1;
                $fileFrom = "/" . $año . "/Semana" . $semana . "/Foto/" . $folio . ".jpg";
                $firma = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_foto);
            }
            $fileFrom = "/" . $año . "/Semana" . $semana . "/Huella2/" . $folio . "2.wsq";
            $fileTo_huella2 = "licencias/" . $folio . "2.wsq";
            $huella_i = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_huella2);
            if ($huella_i == false) {
                $semana = $semana + 1;
                $fileFrom = "/" . $año . "/Semana" . $semana . "/Foto/" . $folio . ".jpg";
                $huella_i = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_foto);
            }
            $fileFrom = "/" . $año . "/Semana" . $semana . "/Huella7/" . $folio . "7.wsq";
            $fileTo_huella7 = "licencias/" . $folio . "7.wsq";
            $huella_d = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_huella7);
            if ($huella_d == false) {
                $semana = $semana + 1;
                $fileFrom = "/" . $año . "/Semana" . $semana . "/Foto/" . $folio . ".jpg";
                $huella_d = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_foto);
            }
            return view('Fotos_k8')
                ->with('folio', $folio)
                ->with('foto', $fileTo_foto)
                ->with('firma', $fileTo_firma)
                ->with('huella_i', $fileTo_huella2)
                ->with('huella_d', $fileTo_huella7);
            //->with('ciudadano', $ciudadano[0]); NO HAY CURP
        } else {
            $ultimo_tramite = \DB::connection('oracle')->select("
          SELECT max(folio) 
          FROM licencias 
          WHERE curp = '" . $request->curp_final . "'
          GROUP BY curp
            ");
            $ultimo_tramite = array_map(function ($item) {
                return (array)$item;
            }, $ultimo_tramite);

            $lugar_fotos = \DB::connection('oracle')->select("
         SELECT to_char(fecha_alta,'WW') semana,to_char(fecha_alta,'YYYY') anio 
         FROM LICENCIAS 
         WHERE FOLIO ='" . $ultimo_tramite[0]["max(folio)"] . "'
            ");
            $lugar_fotos = array_map(function ($item) {
                return (array)$item;
            }, $lugar_fotos);

            $ciudadano = \DB::connection('oracle')->select("
            SELECT * FROM conductores 
            WHERE  curp = '" . $request->curp_final . "'");
            $ciudadano = array_map(function ($item) {
                return (array)$item;
            }, $ciudadano);

            $año = $lugar_fotos[0]['anio'];
            $semana = $lugar_fotos[0]['semana'];
            $folio = $ultimo_tramite[0]["max(folio)"];

            //dd($semana.$folio);
            //dd($ciudadano[0]);
            $fileFrom = "/" . $año . "/Semana" . $semana . "/Foto/" . $folio . ".jpg";
            $fileTo_foto = "licencias/" . $folio . ".jpg";

            $foto = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_foto);

            $fileFrom = "/" . $año . "/Semana" . $semana . "/Firma/" . $folio . ".bmp";
            $fileTo_firma = "licencias/" . $folio . ".bmp";
            $firma = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_firma);

            $fileFrom = "/" . $año . "/Semana" . $semana . "/Huella2/" . $folio . "2.wsq";
            $fileTo_huella2 = "licencias/" . $folio . "2.wsq";
            $huella_i = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_huella2);

            $fileFrom = "/" . $año . "/Semana" . $semana . "/Huella7/" . $folio . "7.wsq";
            $fileTo_huella7 = "licencias/" . $folio . "7.wsq";
            $huella_d = FTP::connection('FTP_licencia')->downloadFile($fileFrom, $fileTo_huella7);

            return view('Fotos_k8')
                ->with('folio', $folio)
                ->with('foto', $fileTo_foto)
                ->with('firma', $fileTo_firma)
                ->with('huella_i', $fileTo_huella2)
                ->with('huella_d', $fileTo_huella7)
                ->with('ciudadano', $ciudadano[0]);
        }


    }

}
