<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\PDF_Controller as PDFC;


class Linea_Captura_Controller extends BaseController
{
    function __construct(PDFC $pdfc)
    {
        $this->PDFC = $pdfc;
        $this->middleware('auth');
    }

    public function inicio(Request $request)
    {
        return view('Lineas_captura');
    }

    public function comprobar(Request $request)
    {

        if (isset($request->archivo_linea_captura)) //Solo archivo
        {
            $input = $request->archivo_linea_captura;
            $destinationPath = '/var/www/alfred/storage/'; // path to save to, has to exist and be writeable
            $fileName = $input->getClientOriginalName(); // original name that it was uploaded with
            $input->move($destinationPath, $fileName); // moving the file to specified dir with the original nam
            $final = $destinationPath . $fileName;


            $a = \Excel::load($final, function ($reader) {
                $reader->noHeading();
            })->get();

            $array_data = array();
            foreach ($a->toArray() as $linea) {

                $data = $this->comprobar_linea_captura_servicio($linea[0]);
                if ($data == null) {
                    $data = $this->comprobar_linea_captura_servicio($linea[0]);
                }
                if ($data == null) {
                    $data = array(
                        "errCode" => 3,
                        'linea' => $linea[0],
                        'cv' => 'LINEA DE CAPTURA NO VERIFICADA',
                        'modulo' => 'LINEA DE CAPTURA NO VERIFICADA',
                        'tramite' => 'LINEA DE CAPTURA NO VERIFICADA',
                        'placa' => 'LINEA DE CAPTURA NO VERIFICADA',
                        'fecha' => 'LINEA DE CAPTURA NO VERIFICADA');

                }

                if ($data != null) {
                    if ($data["errCode"] == 1) {
                        $data['linea'] = $linea[0];
                        $data['cv'] = 'NO UTILIZADA';
                        $data['modulo'] = 'NO UTILIZADA';
                        $data['tramite'] = 'NO UTILIZADA';
                        $data['placa'] = 'NO UTILIZADA';
                        $data['fecha'] = 'NO UTILIZADA';
                    }

                    $array_data[] = $data;
                }

            }

            $ruta = $this->PDFC->reporte_lineas_captura($array_data);


            return view('Lineas_captura')
                ->with('ruta', $ruta)
                ->with('lineas', $array_data);

        } elseif (isset($request->linea_captura)) //Una sola linea de captura
        {

            $array_data = array();


            $data = $this->comprobar_linea_captura_servicio($request->linea_captura);
            if ($data == null) {
                $data = $this->comprobar_linea_captura_servicio($request->linea_captura);
            }
            if ($data == null) {
                $data = array(
                    "errCode" => 3,
                    'linea' => $request->linea_captura,
                    'cv' => 'LINEA DE CAPTURA NO VERIFICADA',
                    'modulo' => 'LINEA DE CAPTURA NO VERIFICADA',
                    'tramite' => 'LINEA DE CAPTURA NO VERIFICADA',
                    'placa' => 'LINEA DE CAPTURA NO VERIFICADA',
                    'fecha' => 'LINEA DE CAPTURA NO VERIFICADA');

            }
            if ($data != null) {
                if ($data["errCode"] == 1) {
                    $data['linea'] = $request->linea_captura;
                    $data['cv'] = 'NO UTILIZADO';
                    $data['modulo'] = 'NO UTILIZADO';
                    $data['tramite'] = 'NO UTILIZADO';
                    $data['placa'] = 'NO UTILIZADO';
                    $data['fecha'] = 'NO UTILIZADO';
                }

                $array_data[] = $data;
            }
            $ruta = $this->PDFC->reporte_lineas_captura($array_data);


            return view('Lineas_captura')
                ->with('ruta', $ruta)
                ->with('lineas', $array_data);


        } else // Quien sabe que paso :v
            return ("No se puede los dos :v");

    }

    public function comprobar_linea_captura_servicio($linea)
    {

        $postData = array(
            "lc" => $linea,
        );
        $postData = json_encode($postData);

        $url = "http://128.222.200.178:8147/buscalc";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $output = json_decode(curl_exec($ch), true);
        //dd($output[0]);
        return $output[0];

    }
}
