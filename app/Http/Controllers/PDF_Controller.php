<?php

namespace App\Http\Controllers;

set_time_limit(0);
ini_set('memory_limit', '512M');

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Dompdf\Dompdf;
use Dompdf\Options;


class PDF_Controller extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function reporte_lineas_captura($data_array)
    {

        $data = $data_array;
        $view = \View::make('PDF/Lineas_Captura_PDF', compact('data'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);

        /* Verlo en el navegador*/
        // return $pdf->stream('invoice', array('Attachment' => 0));

        $output = $pdf->output();
        $rnd_name = rand(0, 1000);
        $nombre_pdf = $rnd_name;
        file_put_contents($nombre_pdf . ".pdf", $output);
        $ruta = "/alfred/PDF/" . $nombre_pdf;
        return $ruta;
    }

    public function bajar_pdf($numero)
    {
        $file = $numero . ".pdf";
        return view('Contenedor_PDF')->with('pdf', $file);

    }

}
