<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;


class Reportes_Controller extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function inicio(Request $request)
    {
        return view('Reportes');
    }

    public function generar_Reporte(Request $request)
    {

        $fecha = explode('-', $request->fecha);
        //dd($fecha);
        $año = $fecha[0];
        $mes = $fecha[1];
        $dia = $fecha[2];


        $tramites_lic = \DB::connection('oracle')->select('select
                    case when TIPO_LIC= \'A1\' then \'Tipo A\'
                         when TIPO_LIC= \'P1\' then \'Permiso\'
                         end as tipo_licencia,
                    case when tipo_mov=\'N\' then \'Nueva\'
                         when tipo_mov=\'C\' then \'Canje\'
                         when tipo_mov=\'R\' then \'Reposicion\'
                         end as tipo_movimineto,
                    count(*) as total
                    from licencias  
                    where EXTRACT(YEAR FROM fecha_captura)=2017
                     AND EXTRACT(MONTH FROM fecha_captura)=06
                     AND EXTRACT(DAY FROM fecha_captura)=04
                    and modulo <> 99
                    group by TIPO_LIC,tipo_mov
                    order by 1');
        $tramites_lic = array_map(function ($item) {
            return (array)$item;
        }, $tramites_lic);

        $tramites = \DB::connection('Informix')->select('
                    select  
                    t.fecmov as fecha
                    ,m.dsc as tramite
                    ,count(*) as total_tramites 
                    from cvehmov t inner join cvehmovto m  
                     on  (t.cvemovto=m.cvemovto and year(t.fecmov)="' . $año . '" and month(t.fecmov)="' . $mes . '" and day(t.fecmov)="' . $dia . '")  
                    group by  t.fecmov,m.dsc
                    order by t.fecmov asc , m.dsc asc;');

        $tramites = array_map(function ($item) {
            return (array)$item;
        }, $tramites);
        //$tramites = null;


        return view('Reportes')
            ->with('tramites', $tramites)
            ->with('tramites_lic', $tramites_lic);

    }

}
