<?php

namespace App\Http\Controllers\Repuve;

use App\Http\Controllers\Controller;
use App\Services\REPUVEService;
use PDF;

class ConsultaController extends Controller
{
    private $repuve;

    public function __construct(REPUVEService $repuve)
    {
        $this->middleware('auth');
        date_default_timezone_set('America/Mexico_City');
        $this->repuve = $repuve;
    }

    function consultPlate($plate)
    {
        return $this->repuve->getInformationByPlate($plate);
    }

    function stolePlate($plate)
    {
        return $this->repuve->verifyStoleReportByPlate($plate);
    }

    function consultVIN($vin)
    {
        return $this->repuve->getInformationByVIN($vin);
    }

    function stoleVIN($vin)
    {
        return $this->repuve->verifyStoleReportByVIN($vin);
    }

    function pdf($consult)
    {
        $resp = self::consultPlate($consult);

        $data = array();

        $data['marca'] = $resp[18];
        $data['modelo'] = $resp[16];
        $data['año'] = $resp[14];
        $data['clase'] = $resp[21];
        $data['niv'] = $resp[3];
        $data['nci'] = $resp[2];
        $data['placa'] = $resp[12];
        $data['version'] = $resp[16];
        $data['instituto'] = $resp[1];
        $data['fec_ins'] = $resp[4];
        $data['hor_ins'] = $resp[5];
        $data['entidad'] = $resp[17];
        $data['fec_emp'] = $resp[9];
        $data['obs'] = $resp[31];


        $robo = self::stolePlate($consult);

        if (explode(':', $robo[0])[0] === 'ERR') {
            $data['robo'] = 0;
        } else {
            $data['robo'] = 1;

            $data['fec_rob'] = $robo[1];
            $data['fuen_rob'] = $robo[4];
            $data['av'] = $robo[5];
            $data['mp'] = $robo[6];
            $data['fec_av'] = $robo[7];
        }

        $pdf = PDF::loadView('repuve.pdf', $data);

        return $pdf->stream('REPUVE.pdf');
    }
}
