<?php

namespace App\Http\Controllers\Repuve;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function repuve()
    {
        return view('repuve.repuve');
    }
}
