<?php

namespace App\Http\Controllers\SheetLicense;

use App\Http\Controllers\Controller;
use App\Services\LicenseService;
use Illuminate\Http\Request;
use PDF;

class ConsultController extends Controller
{

    private $url = "http://128.222.200.41:7777/alfred/licenses/type/";

    //private $url = "http://10.5.1.29:7777/alfred/licenses/type/";

    public function __construct()
    {
        $this->middleware('auth');
        date_default_timezone_set('America/Mexico_City');
    }

    function ConsultName(Request $request, LicenseService $license)
    {
        $name = \Manipulator::removeSpecialChars(strtoupper($request->input('name')));

        $first_surname = \Manipulator::removeSpecialChars(strtoupper($request->input('first_surname')));

        $second_surname = \Manipulator::removeSpecialChars(strtoupper($request->input('second_surname')));

        \Binnacle::save("Lic_Nombre", $name . " " . $first_surname . " " . $second_surname);

        $body = array(
            'service' => 'name',
            'owner' => array(
                'name' => $name,
                'first_surname' => $first_surname,
                'second_surname' => $second_surname
            )
        );

        $group_licenses = $license->getAllLicenses($this->url, $body);

        if (empty($group_licenses)) {
            session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

            return redirect(route('Sheet_License_Name'));
        } else {
            return view('SheetLicense.results')
                ->with(array(
                    'licenses' => $group_licenses,
                    'search' => strtoupper($request->input('name')) . ' ' . strtoupper($request->input('first_surname')) . ' ' . strtoupper($request->input('second_surname'))
                ));
        }
    }

    function ConsultCurp(Request $request, LicenseService $license)
    {
        $curp = strtoupper($request->input('curp'));

        \Binnacle::save('Lic_CURP', $curp);

        $body = array(
            'service' => 'curp',
            'owner' => array(
                'curp' => $curp
            )
        );

        $group_licenses = $license->getAllLicenses($this->url, $body);

        if (empty($group_licenses)) {
            session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

            return redirect(route('Sheet_License_CURP'));
        } else {
            return view('SheetLicense.results')
                ->with(array(
                    'licenses' => $group_licenses,
                    'search' => $curp
                ));
        }
    }

    function ConsultFolio(Request $request)
    {
        $folio = $request->input('folio');
        $search = $request->input('search') ?? $folio;
        $type = $request->input('type');

        \Binnacle::save("Lic_Folio", $folio);

        $url = $this->url . $type;

        $body = array(
            'service' => 'folio',
            'folio' => $folio
        );

        $folio_search = \Consumer::consume($url, json_encode($body));

        if ($folio_search->errCode == "0") {
            return $this->MakePDF($folio_search, $type, $search);
        } else {
            return redirect(route('error-500'));
        }
    }

    function MakePDF($search, $type, $previous)
    {
        $data = array();

        \Binnacle::save("Lic_" . $type, $search->folio);

        $data['previous'] = $previous;

        $data['name'] = $search->name . " " . $search->first_surname . " " . $search->second_surname;

        if ($type == 'A') {
            if (strtotime($search->registry[0]->date_begin) >= strtotime("01-10-2017")) {
                $data['photo'] = $this->New_Photo($search->registry[0]->folio);
            } else {
                $data['photo'] = $this->Photo($search->registry[0]->folio, $search->registry[0]->date_begin);
            }
        } elseif ($type == 'B'){
            $data['photo'] = '';
        } else {
            $data['photo'] = $search->photo;
        }

        $data['curp'] = $search->curp;
        $data['folio'] = $search->folio;
        $data['street'] = 'CALLE: ' . $search->street;
        $data['ext'] = 'NO EXT: ' . $search->exterior_number;
        $data['int'] = 'NO INT: ' . $search->interior_number;
        $data['suburb'] = 'COL: ' . strtoupper($search->suburb);
        $data['mayoralty'] = 'DELG: ' . strtoupper($search->mayoralty);
        $data['telephone'] = $search->telephone;
        $data['module'] = $search->module;
        $data['date'] = $search->date;
        $data['hour'] = $search->hour;
        $data['registry'] = $search->registry;

        $url = $this->url . $type;

        $body = array(
            'service' => 'padlock',
            'owner' => array(
                'name' => $search->name,
                'first_surname' => $search->first_surname,
                'second_surname' => $search->second_surname,
                'curp' => $search->curp
            )
        );

        $data['padlocks'] = \Consumer::consume($url, json_encode($body));

        $pdf_name = $search->curp . '.pdf';

        $pdf = PDF::loadView('SheetLicense.pdf_template', $data);

        return $pdf->stream($pdf_name);
    }

    function Photo($folio, $fecha)
    {
        $foli = $folio . ".jpg";

        $year = date("Y", strtotime($fecha));
        $week = (int)ltrim(date("W", strtotime($fecha)), '0');

        try {
            $path = "/" . $year . "/Semana" . ($week) . "/Foto/" . $foli;

            $connect = ftp_connect("128.222.200.213", '20');
            $login = ftp_login($connect, "Administrador", "Semovi201727");

            ftp_pasv($connect, true);

            if (ftp_get($connect, $foli, $path, FTP_BINARY)) {
                ftp_quit($connect);
                $data = file_get_contents($foli);
                unlink($foli);
                return base64_encode($data);
            }
        } catch (\Exception $exception) {
        }

        try {
            $path = "/" . $year . "/Semana" . ($week + 1) . "/Foto/" . $foli;

            $connect = ftp_connect("128.222.200.213", '20');
            $login = ftp_login($connect, "Administrador", "Semovi201727");

            ftp_pasv($connect, true);

            if (ftp_get($connect, $foli, $path, FTP_BINARY)) {
                ftp_quit($connect);
                $data = file_get_contents($foli);
                unlink($foli);
                return base64_encode($data);
            }
        } catch (\Exception $exception) {
        }

        try {
            $path = "/" . $year . "/Semana" . ($week + 2) . "/Foto/" . $foli;

            $connect = ftp_connect("128.222.200.213", '20');
            $login = ftp_login($connect, "Administrador", "Semovi201727");

            ftp_pasv($connect, true);

            if (ftp_get($connect, $foli, $path, FTP_BINARY)) {
                ftp_quit($connect);
                $data = file_get_contents($foli);
                unlink($foli);
                return base64_encode($data);
            }
        } catch (\Exception $exception) {
        }

        try {
            $path = "/" . $year . "/Semana" . ($week - 1) . "/Foto/" . $foli;

            $connect = ftp_connect("128.222.200.213", '20');
            $login = ftp_login($connect, "Administrador", "Semovi201727");

            ftp_pasv($connect, true);

            if (ftp_get($connect, $foli, $path, FTP_BINARY)) {
                ftp_quit($connect);
                $data = file_get_contents($foli);
                unlink($foli);
                return base64_encode($data);
            }
        } catch (\Exception $exception) {
        }

        try {
            $path = "/" . $year . "/Semana" . ($week - 2) . "/Foto/" . $foli;

            $connect = ftp_connect("128.222.200.213", '20');
            $login = ftp_login($connect, "Administrador", "Semovi201727");

            ftp_pasv($connect, true);

            if (ftp_get($connect, $foli, $path, FTP_BINARY)) {
                ftp_quit($connect);
                $data = file_get_contents($foli);
                unlink($foli);
                return base64_encode($data);
            }
        } catch (\Exception $exception) {
        }
    }

    function New_Photo($folio)
    {
        try {
            $url = $this->url . 'A';

            $body = array(
                'service' => 'photo',
                'folio' => $folio
            );

            $path = \Consumer::consume($url, json_encode($body));

            if ($path[0]->errCode == 0) {

                $path = $path[0]->path;

                //Port 21
                $connect = ftp_connect("128.222.200.213", '21');
                $login = ftp_login($connect, "administrador", "Semovi201727");

                ftp_pasv($connect, true);

                if (ftp_get($connect, $folio, $path, FTP_BINARY)) {
                    ftp_quit($connect);
                    $data = file_get_contents($folio);
                    unlink($folio);
                    return base64_encode($data);
                } else {
                    return '';
                }
            }
        } catch (\Exception $exception) {
            return '';
        }
    }
}