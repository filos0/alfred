<?php

namespace App\Http\Controllers\SheetLicense;

use App\Http\Controllers\Controller;
use Session;

class RoutesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Name()
    {
        return view('SheetLicense.search_name');
    }

    public function CURP()
    {
        return view('SheetLicense.search_curp');
    }

    public function Folio()
    {
        return view('SheetLicense.search_folio');
    }
}
