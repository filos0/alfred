<?php

namespace App\Http\Controllers\SheetVehicle;

ini_set('max_execution_time', 300);

use App\Http\Controllers\Controller;
use App\Services\VehicleService;
use Auth;
use Illuminate\Http\Request;
use PDF;

class ConsultController extends Controller
{

    private $url = "http://128.222.200.41:7777/alfred/vehicles/type/";

    //private $url = "http://10.5.1.29:7777/alfred/vehicles/type/";

    /*
     * Consultar Sabana Vehicular por RFC
    */
    public function ConsultCURP_RFC(Request $request, VehicleService $vehicleService)
    {
        $curp = $request->input('curp');

        \Binnacle::save('Veh_RFC', $curp);

        $body = array(
            'service' => 'curp',
            'owner' => array(
                'curp' => $curp
            )
        );

        $vehicles = $vehicleService->getAllVehicles($this->url, $body);

        if (empty($vehicles)) {
            session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

            return redirect(route('Sheet_Vehicle_CURP'));
        } else {
            return view('SheetVehicle.results')->with(array(
                'results' => $vehicles
            ));
        }
    }

    /*
     * Consultar Sabana Vehicular por Nombre
    */
    public function ConsultName(Request $request, VehicleService $vehicleService)
    {
        $name = \Manipulator::removeSpecialChars(strtoupper($request->input('name')));

        $first_surname = \Manipulator::removeSpecialChars(strtoupper($request->input('first_surname')));

        $second_surname = \Manipulator::removeSpecialChars(strtoupper($request->input('second_surname')));

        \Binnacle::save('Veh_Nombre', $name . " " . $first_surname . " " . $second_surname);

        $body = array(
            'service' => 'name',
            'owner' => array(
                'name' => $name,
                'first_surname' => $first_surname,
                'second_surname' => $second_surname
            )
        );

        $vehicles = $vehicleService->getAllVehicles($this->url, $body);

        if (empty($vehicles)) {
            session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

            return redirect(route('Sheet_Vehicle_Name'));
        } else {
            return view('SheetVehicle.results')->with(array(
                'results' => $vehicles
            ));
        }
    }

    /*
     * Consultar Sabana Vehicular por Razon Social
    */
    public function ConsultBusiness(Request $request, VehicleService $vehicleService)
    {
        $business_name = \Manipulator::removeSpecialChars(strtoupper($request->input('business_name')));

        \Binnacle::save('Veh_Razon', $business_name);

        $body = array(
            'service' => 'business',
            'owner' => array(
                'business_name' => $business_name
            )
        );

        $vehicles = $vehicleService->getAllVehicles($this->url, $body);

        if (empty($vehicles)) {
            session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

            return redirect(route('Sheet_Vehicle_Business'));
        } else {
            return view('SheetVehicle.results')->with(array(
                'results' => $vehicles
            ));
        }
    }

    /*
     * Consultar Sabana Vehicular por Placa
    */
    public function ConsultPlate(Request $request, VehicleService $vehicleService)
    {
        $plate = strtoupper($request->input('plate'));

        $type = $request->input('type');

        \Binnacle::save('Veh_Placa', $plate);

        if (Auth::user()->hasRole('usr_veh_' . $type)) {
            $url_search = $this->url . $type;

            $body = array(
                'service' => 'vehicle',
                'vehicle' => array(
                    'plate' => $plate
                )
            );

            $vehicle_search = \Consumer::consume($url_search, json_encode($body));

            $vehicle = array();

            foreach ($vehicle_search as $vehicles) {
                if ($vehicles->errCode == 0) {
                    $vehicle[] = $vehicles;
                }
            }

            if (empty($vehicle)) {
                return redirect(route('error-500'));
            }

            usort($vehicle, function ($a, $b) {
                return strtotime($a->date_up) - strtotime($b->date_up);
            });

            $vehicle = end($vehicle);

            if ($type != 'verde') {
                $body = array(
                    'service' => 'circulation_card',
                    'vehicle' => array(
                        'plate' => $plate
                    )
                );

                $circulation_cards = \Consumer::consume($url_search, json_encode($body));

                $cards = array();

                foreach ($circulation_cards as $card) {
                    if ($card->errCode == 0) {
                        $cards[] = $card;
                    }
                }

                if (empty($cards)) {
                    return response()->view('errors.1000', [], 500);
                }

                usort($cards, function ($a, $b) {
                    return strtotime($a->expedition_date) - strtotime($b->expedition_date);
                });

                $cards = end($cards);

                /////////////////////////////////////////////////////////////////////////////////

                $body = array(
                    'service' => 'padlock',
                    'vehicle' => array(
                        'plate' => $plate,
                        'vin' => $vehicle->vin
                    )
                );

                $padlock = \Consumer::consume($url_search, json_encode($body));

                /////////////////////////////////////////////////////////////////////////////////

                if ($type == 'taxi') {
                    $vehicular_key = $vehicleService->consultVehicularKey($vehicle->vehicular_key);
                } else {
                    $vehicular_key = $vehicleService->getVehicularKey($plate, $vehicle->vehicular_key, $url_search);
                }

                return $this->Sheet($vehicle, $cards, $padlock, $vehicular_key, strtoupper($type));
            }

            $body = array(
                'service' => 'padlock',
                'vehicle' => array(
                    'plate' => $plate,
                    'vin' => $vehicle->vin
                )
            );

            $padlock = \Consumer::consume($url_search, json_encode($body));

            return $this->GreenSheet($vehicle, $padlock);
        }
    }

    /*
     * Consultar Sabana Vehicular por SerieVh
    */
    public function ConsultVIN(Request $request, VehicleService $vehicleService)
    {
        $vin = strtoupper($request->input('vin'));

        \Binnacle::save('Veh_Serie', $vin);

        $body = array(
            'service' => 'vin',
            'vehicle' => array(
                'vin' => $vin
            )
        );

        $vehicles = $vehicleService->getAllVehicles($this->url, $body);

        if (empty($vehicles)) {
            session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

            return redirect(route('Sheet_Vehicle_VIN'));
        } else {
            return view('SheetVehicle.results')->with(array(
                'results' => $vehicles
            ));
        }
    }

    /*
     * Consultar Sabana Vehicular por Folio
    */
    public function ConsultFolio(Request $request)
    {
        $folio = strtoupper($request->input('folio'));

        $type = $request->input('type');

        \Binnacle::save('Veh_Folio', $folio);

        if (Auth::user()->hasRole('usr_veh_' . $type)) {
            $url_search = $this->url . $type;

            $body = array(
                'service' => 'folio',
                'owner' => array(
                    'folio' => $folio
                )
            );

            $vehicle_search = \Consumer::consume($url_search, json_encode($body));

            $vehicles = array();

            foreach ($vehicle_search as $vehicle) {
                if ($vehicle->errCode == 0) {
                    $vehicles[] = $vehicle;
                }
            }

            if (!empty($vehicles)) {
                return view('SheetVehicle.results')->with(array(
                    'results' => $vehicles
                ));
            }
        }

        session()->flash('error', 'No se han encontrado resultados\nVerifique su busqueda, gracias!');

        return redirect(route('Sheet_Vehicle_Folio'));
    }

    public function Sheet($vehicle, $card, $padlock, $key, $type)
    {
        $data = array();

        $data['type'] = $type;
        $data['name'] = $card->owner;
        $data['curp'] = $card->curp;
        $data['address'] = $card->address;
        $data['suburb'] = $card->suburb;
        $data['mayoralty'] = $card->mayoralty;
        $data['sex'] = $card->sex;
        $data['state'] = $card->state;
        $data['postal_code'] = $card->postal_code;
        $data['telephone'] = $card->telephone;
        $data['vin'] = $vehicle->vin;
        $data['plate'] = $vehicle->plate;
        $data['class'] = $vehicle->class;
        $data['mark'] = $key->mark;
        $data['model'] = $vehicle->model;
        $data['line'] = $key->line;
        $data['type_vehicle'] = $vehicle->vehicle_type;
        $data['version'] = $key->model;
        $data['type_service'] = $vehicle->service_type;
        $data['engine_number'] = $vehicle->motor_number;
        $data['fuel'] = $vehicle->type_gas;
        $data['use'] = $vehicle->use;
        $data['policy'] = $vehicle->insurance_policy;
        $data['repuve'] = $vehicle->repuve;
        $data['vehicular_key'] = $vehicle->vehicular_key;
        $data['discharge_date'] = $vehicle->date_up;
        $data['bill'] = $vehicle->invoice_value;
        $data['number_doors'] = $vehicle->number_doors;

        if ($type == 'PARTICULAR') {
            $data['previous_plate'] = $card->previous_plate;
        } else {
            $data['previous_plate'] = $vehicle->previous_plate;
        }

        if ($type == 'PARTICULAR' || $type == 'ANTIGUO' || $type == 'MOTO' || $type == 'REMOLQUE' || $type == 'ESCOLTA') {
            $url_search = $this->url . strtolower($type);

            $body = array(
                'service' => 'movement',
                'vehicle' => array(
                    'plate' => $vehicle->plate
                )
            );

            $move = \Consumer::consume($url_search, json_encode($body));

            if ($move[0]->errCode == 0) {
                foreach ($move as $mov) {
                    if ($mov->movement == 'ALTA' || $mov->movement == 'ALTA USADOS') {
                        $data['usr_alta'] = $mov->user_up;
                    } else {
                        $data['usr_alta'] = "INDEFINIDO";
                    }
                }

                $move = $move[0];

                $data['qr'] = $move->folio_qr;
                $data['movement'] = $move->movement;
                $data['module'] = $move->module;
                $data['usr_mov'] = $move->user_movement;
                $data['date_movement'] = $move->date_movement;
            } else {
                $data['qr'] = 'NO';
                $data['movement'] = 'NO';
                $data['module'] = 'NO';
                $data['usr_mov'] = 'NO';
                $data['date_movement'] = 'NO';
                $data['usr_alta'] = 'NO';
            }
        } else {
            $data['qr'] = 'NO';
            $data['movement'] = 'NO';
            $data['module'] = 'NO';
            $data['usr_mov'] = 'NO';
            $data['date_movement'] = 'NO';
            $data['usr_alta'] = 'NO';
        }

        $data['number_passenger'] = $vehicle->number_passenger;
        $data['folio_circulation_card'] = $card->circulation_card_number;
        $data['number_cylinders'] = $vehicle->number_cylinders;
        $data['status_vehicle'] = $vehicle->status;
        $data['status_card'] = $card->status;
        $data['padlocks'] = $padlock;
        $data['user'] = Auth::user()->username;

        $pdf = PDF::loadView('SheetVehicle.template', $data);

        $name = $data['plate'] . ".pdf";

        return $pdf->stream($name);
    }

    public function GreenSheet($vehicle, $padlock)
    {
        $data = array();

        $data['type'] = 'PLACA VERDE';
        $data['name'] = $vehicle->owner;
        $data['curp'] = $vehicle->curp;
        $data['address'] = $vehicle->address;
        $data['suburb'] = $vehicle->suburb;
        $data['mayoralty'] = $vehicle->mayoralty;
        $data['sex'] = $vehicle->sex;
        $data['state'] = $vehicle->state;
        $data['postal_code'] = $vehicle->postal_code;
        $data['telephone'] = $vehicle->telephone;
        $data['vin'] = $vehicle->vin;
        $data['plate'] = $vehicle->plate;
        $data['class'] = $vehicle->class;
        $data['mark'] = $vehicle->mark;
        $data['model'] = $vehicle->model;
        $data['line'] = $vehicle->line;
        $data['type_vehicle'] = $vehicle->vehicle_type;
        $data['version'] = $vehicle->model;
        $data['type_service'] = $vehicle->service_type;
        $data['engine_number'] = $vehicle->motor_number;
        $data['fuel'] = $vehicle->type_gas;
        $data['use'] = $vehicle->use;
        $data['policy'] = $vehicle->insurance_policy;
        $data['repuve'] = $vehicle->repuve;
        $data['vehicular_key'] = $vehicle->vehicular_key;
        $data['discharge_date'] = $vehicle->date_up;
        $data['bill'] = $vehicle->invoice_value;
        $data['number_doors'] = $vehicle->number_doors;

        $data['previous_plate'] = 'NO';

        $url_search = $this->url . 'verde';

        $body = array(
            'service' => 'movement',
            'vehicle' => array(
                'plate' => $vehicle->plate
            )
        );

        $move = \Consumer::consume($url_search, json_encode($body));

        if ($move[0]->errCode == 0) {
            foreach ($move as $mov) {
                if ($mov->movement == 'ALTA' || $mov->movement == 'ALTA USADOS' || $mov->movement == 'ALTA NUEVO') {
                    $data['usr_alta'] = $mov->user_up;
                } else {
                    $data['usr_alta'] = "INDEFINIDO";
                }
            }

            $move = $move[0];

            $data['qr'] = $move->folio_qr;
            $data['folio_circulation_card'] = $move->folio_qr;
            $data['movement'] = $move->movement;
            $data['module'] = $move->module;
            $data['usr_mov'] = $move->user_movement;
            $data['date_movement'] = $move->date_movement;
        } else {
            $data['qr'] = 'NO';
            $data['movement'] = 'NO';
            $data['module'] = 'NO';
            $data['usr_mov'] = 'NO';
            $data['date_movement'] = 'NO';
            $data['usr_alta'] = 'NO';
        }

        $data['number_passenger'] = $vehicle->number_passenger;
        $data['number_cylinders'] = $vehicle->number_cylinders;
        $data['status_vehicle'] = $vehicle->status;
        $data['status_card'] = $vehicle->status;
        $data['padlocks'] = $padlock;
        $data['user'] = Auth::user()->username;

        $pdf = PDF::loadView('SheetVehicle.template', $data);

        $name = $data['plate'] . ".pdf";

        return $pdf->stream($name);
    }
}
