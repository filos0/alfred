<?php

namespace App\Http\Controllers\Tenure;

use App\Http\Controllers\Controller;
use App\Interfaces\ConsumeInterface;
use App\Services\VehicleService;
use Illuminate\Http\Request;

class TenureController extends Controller
{
    public function CheckTenure(Request $request, VehicleService $vehicleService)
    {
        $plate = strtoupper($request->input('plate'));

        \Binnacle::save('Tenure', $plate);

        $valid_tenure = $vehicleService->getTenures($plate);

        return view('tenure.show')->with('tenures', $valid_tenure);
    }
}
