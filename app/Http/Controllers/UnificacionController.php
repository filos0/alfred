<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Illuminate\Routing\Controller as BaseController;
use DB;


class UnificacionController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function inicio()
    {
        //Invocando vista
        return view('unificar_tram');
    }

    public function buscar(Request $request)
    {
        //dd($request);
        $validator = Validator::make($request->toArray(),
            [
                'curp' => 'required',
            ],

            [
                'curp.required' => 'Debe llenar todos los campos',
            ]
        );

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }


        $curp = \DB::connection('oracle')->select('select * from CONDUCTORES 
                    where  CURP = \'' . $request->curp . '\'');
        if (isset($curp[0])) {
            if ($n = count($curp) > 1) {
                $error = 'Exixten ' . $n . ' Conductores';
                return redirect()->back()->withErrors($error);
            } else {

                $nombre = $curp;
                $error = 'Se encontro el CURP  ' . $request->curp . ' relacionado al nombre :  ' . $nombre[0]->nombre . ' ' . $nombre[0]->paterno . ' ' . $nombre[0]->materno;
                return view('unificar')->withErrors($error)->with('curp', $request->curp);
            }
        } else {
            $error = 'Verifique que exista el CURP o RFC';
            return redirect()->back()->withErrors($error);
        }

    }

    public function UniTramite(Request $request)
    {

        function multiexplode($delimiters, $string)
        {
            $ready = str_replace($delimiters, $delimiters[0], $string);
            $launch = explode($delimiters[0], $ready);
            return $launch;
        }

        $text = $request->folio;
        $exploded = multiexplode(array(",", "\r\n",), $text);
        $n = 0;
        foreach ($exploded as $f) {

            if (\DB::connection('oracle')->table('LICENCIAS')->Where('FOLIO', '=', $f)->Update(array('CURP' => $request->curp)) == 0)
                $n++;

        }
        if (count($n) > 0) {

            $error = 'Ocurrieron ' . $n . ' errores, intente ingresando uno por uno los folios';
        } else {
            $error = 'Unificacion Exitosa';
        }

        return view('unificar')->withErrors($error)->with('curp', $request->curp);

    }


}