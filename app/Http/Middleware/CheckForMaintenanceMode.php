<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;

class CheckForMaintenanceMode
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Application::getInstance()->isDownForMaintenance() && !in_array($request->ip(), ['10.5.1.29'])) {
            return response(view('errors.503'), 503);
        }

        return $next($request);
    }
}
