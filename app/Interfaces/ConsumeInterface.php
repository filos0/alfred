<?php

namespace App\Interfaces;

interface ConsumeInterface
{
    public function consume($url, $body);
}