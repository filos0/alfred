<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BitacoraAlfred extends Model
{
    protected  $connection = "auth";

    protected $fillable = [
      'user_id', 'tipo', 'busqueda'
    ];

}
