<?php

namespace App\Providers;

use App\Services\BinnacleService;
use App\Services\ConsumeViaCURLService;
use App\Services\LicenseService;
use App\Services\ManipulatorService;
use App\Services\REPUVEService;
use App\Services\VehicleService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('ConsumeInterface', function () {
            return new ConsumeViaCURLService;
        });

        $this->app->bind('Binnacle', function () {
            return new BinnacleService;
        });

        $this->app->bind('Manipulator', function () {
            return new ManipulatorService;
        });

        $this->app->bind('App\Services\LicenseService', function () {
            return new LicenseService();
        });

        $this->app->bind('App\Services\VehicleService', function () {
            return new VehicleService;
        });

        $this->app->bind('App\Services\REPUVEService', function () {
            return new REPUVEService;
        });

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
