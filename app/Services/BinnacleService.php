<?php

namespace App\Services;

use App\Models\BitacoraAlfred;

class BinnacleService
{
    public function save($type, $search)
    {
        $binnacle = new BitacoraAlfred();
        $binnacle->user_id = \Auth::id();
        $binnacle->tipo = $type;
        $binnacle->busqueda = $search;
        $binnacle->save();
    }
}