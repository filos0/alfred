<?php

namespace App\Services;

use App\Interfaces\ConsumeInterface;

class ConsumeViaCURLService implements ConsumeInterface
{
    public function consume($url, $body)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(

            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 7d4ec8aa-7e00-0663-5e0d-84ef4d8db375"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode(utf8_encode($response));

        if(empty($response)){
            $arr = new \stdClass();
            $arr->errCode = 1;
            $response = null;
            $response[0] = $arr;
        }

        return $response;
    }
}