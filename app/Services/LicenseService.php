<?php

namespace App\Services;

class LicenseService
{
    function deleteDuplicatesLicenses($licenses)
    {
        $uniques = array_unique(array_column($licenses, 'curp'));
        $unique_licenses = array();
        foreach ($uniques as $unique) {
            foreach ($licenses as $license) {
                if ($license->curp == $unique) {
                    $unique_licenses["$unique"] = $license;
                }
            }
        }
        return $unique_licenses;
    }

    function getAllLicenses($url, $body)
    {
        $group_licenses = array();

        for ($permission = 'a'; $permission < 'f'; $permission++) {
            if (\Auth::user()->hasRole('usr_lic_' . $permission)) {
                $url_search = $url . strtoupper($permission);

                $license = \Consumer::consume($url_search, json_encode($body));

                if ($license[0]->errCode == 0) {
                    foreach (self::deleteDuplicatesLicenses($license) as $unique) {
                        $group_licenses[] = $unique;
                    }
                }
            }
        }

        return $group_licenses;
    }

    function verifyCaptureLine($url, $body)
    {
        $group_lines = array();

        for ($permission = 'a'; $permission < 'f'; $permission++) {
            $url_search = $url . strtoupper($permission);

            $line = \Consumer::consume($url_search, json_encode($body));

            if ($line->errCode == 0) {
                $group_lines = (array) $line;
                $group_lines += ['type' => $permission];
            }
        }

        return $group_lines;
    }
}