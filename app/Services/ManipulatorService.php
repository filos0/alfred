<?php
/**
 * Created by PhpStorm.
 * User: Meca_SEMOVI
 * Date: 27/08/2018
 * Time: 08:15 PM
 */

namespace App\Services;


class ManipulatorService
{
    function removeSpecialChars($word)
    {
        $exclude = array("ñ", "Ñ", "Á", "á", "É", "é", "Í", "í", "Ó", "ó", "Ú", "ú", " ");

        if (empty($word)) {
            $word = "X";
        } else {
            $word = str_replace($exclude, "_", $word);
        }

        return $word;
    }
}