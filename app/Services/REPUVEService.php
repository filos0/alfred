<?php
/**
 * Created by PhpStorm.
 * User: Meca_SEMOVI
 * Date: 28/08/2018
 * Time: 08:34 PM
 */

namespace App\Services;

class REPUVEService
{
    function getInformationByPlate($plate)
    {
        \Binnacle::save('REPUVE_Plate', $plate);
        $body = array(
            'cadena' => '||' . $plate . '|||||'
        );
        $information = \Consumer::consume('http://10.5.128.1:8100/repuve/consulta/placa', json_encode($body));
        $data = explode('|', $information->return);
        //dd($datos);
        return $data;
    }

    function verifyStoleReportByPlate($plate)
    {
        \Binnacle::save('REPUVE_Stole_Plate', $plate);
        $body = array(
            'cadena' => '||' . $plate . '|||||'
        );
        $information = \Consumer::consume('http://10.5.128.1:8100/repuve/robo/placa', json_encode($body));
        $data = explode('|', $information->return);
        //dd($datos);
        return $data;
    }

    function getInformationByVIN($vin)
    {
        \Binnacle::save('REPUVE_VIN', $vin);
        $body = array(
            'cadena' => $vin . '|||||||'
        );
        $information = \Consumer::consume('http://10.5.128.1:8100/repuve/consulta/serie', json_encode($body));
        $data = explode('|', $information->return);
        //dd($datos);
        return $data;
    }

    function verifyStoleReportByVIN($vin)
    {
        \Binnacle::save('REPUVE_Stole_VIN', $vin);
        $body = array(
            'cadena' => $vin . '|||||||'
        );
        $information = \Consumer::consume('http://10.5.128.1:8100/repuve/robo/serie', json_encode($body));
        $data = explode('|', $information->return);
        //dd($datos);
        return $data;
    }
}