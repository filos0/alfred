<?php

namespace App\Services;

class VehicleService
{
    function consultVehicularKey($key)
    {
        $body = array(
            'numero' => $key
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(

            CURLOPT_URL => 'http://128.222.200.178:8252/ClaveVehicularFinanzas',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 100,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 7d4ec8aa-7e00-0663-5e0d-84ef4d8db375"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode(utf8_encode($response));

        if (empty($response)) {
            $arr = new \stdClass();
            $arr->claveCarga = "null";
            $response = null;
            $response = $arr;
        }

        $key_search = $response;

        $description = new \stdClass();
        if (empty($key_search) || $key_search->claveCarga == "null") {
            $description->line = 'INDEFINIDO';
            $description->model = 'INDEFINIDO';
            $description->mark = 'INDEFINIDO';
        } else {
            $description->line = $key_search->descripcionLinea;
            $description->model = $key_search->descripcionModelo;
            $description->mark = $key_search->descripcionMarca;
        }

        return $description;
    }

    function getAllVehicles($url, $body)
    {
        $types = array(
            'particular',
            'taxi',
            'antiguo',
            'moto',
            'remolque',
            'escolta',
            'micro',
            'carga',
            'verde'
        );

        $vehicules = array();

        foreach ($types as $type) {
            if (\Auth::user()->hasRole('usr_veh_' . $type)) {
                $url_search = $url . $type;

                $vehicule_search = \Consumer::consume($url_search, json_encode($body));

                foreach ($vehicule_search as $vehicule) {
                    if ($vehicule->errCode == 0) {
                        $vehicules[] = $vehicule;
                    }
                }
            }
        }

        return $vehicules;
    }

    function getVehicularKey($plate, $vehicular_key, $url)
    {
        $key = self::ConsultVehicularKey($vehicular_key);

        if ($key->line == 'INDEFINIDO') {
            $body = array(
                'service' => 'vehicular_key',

                'vehicle' => array(
                    'plate' => $plate
                )
            );

            $key = \Consumer::consume($url, json_encode($body));

            return $key[0];
        }

        return $key;
    }

    function getTenures($plate)
    {
        $body = array(
            "placa" => $plate,
            "usuario" => "setravi_ap",
            "password" => "fe83b3c184b8ee1df5034314b542a94e"
        );

        $tenure_collection = \Consumer::consume("http://10.1.126.4/fut_2017/utilerias/wsSEMOVI/webservice_consulta.php/consultar_tenencia", json_encode($body));

        usort($tenure_collection, function ($a, $b) {
            return strtotime($a->perini) - strtotime($b->perini);
        });

        return $tenure_collection;
    }

    function verifyTenures($plate)
    {
        $tenures = self::getTenures($plate);

        if (count($tenures) > 0) {

            $body = array(
                'service' => 'vehicle',
                'vehicle' => array(
                    'plate' => $plate
                )
            );

            $vehicle = \Consumer::consume('http://128.222.200.41:7777/alfred/vehicles/type/particular', json_encode($body));

            $body = array(
                'service' => 'circulation_card',
                'vehicle' => array(
                    'plate' => $plate
                )
            );

            $circulation = \Consumer::consume('http://128.222.200.41:7777/alfred/vehicles/type/particular', json_encode($body));

            $tenures = array_column($tenures, 'perini');

            if ((date('o', strtotime($circulation[0]->expedition_date)) - date('o', strtotime($vehicle[0]->date_up)) + 1) == sizeof($tenures)) {
                $a = 0;

                for ($i = date('o', strtotime($vehicle[0]->date_up)); $i <= date('o', strtotime($circulation[0]->expedition_date)); $i++) {
                    if ($i != $tenures[$a]) {
                        return false;
                    }

                    $a++;
                }

                return true;
            }
        }

        return false;
    }

    function setDownStatus($plate)
    {
        $body = array(
            'service' => 'status_b',
            'vehicle' => array(
                'plate' => $plate
            )
        );

        $set_down = \Consumer::consume('http://128.222.200.41:7777/alfred/vehicles/type/particular', json_encode($body));

        if ($set_down->errCode == 0) {
            return true;
        }

        return false;
    }

    function verifyCaptureLine($url, $body)
    {
        $types = array(
            'particular',
            'taxi',
            'antiguo',
            'moto',
            'remolque',
            'escolta',
            'micro',
            'carga',
            'verde'
        );

        $group_lines = array();

        foreach ($types as $type) {
            switch ($type) {
                case 'taxi':
                    $versions = array(
                        'taxi',
                        '2017',
                        '2018'
                    );
                    foreach ($versions as $version) {
                        $body['procedure']['version'] = $version;
                        $url_search = $url . $type;

                        $line = \Consumer::consume($url_search, json_encode($body));

                        if ($line->errCode == 0) {
                            $group_lines = (array)$line;
                            if ($version == 'taxi') {
                                $group_lines += ['type' => $type, 'version' => 'No'];
                            } else {
                                $group_lines += ['type' => $type, 'version' => $version];
                            }
                        }
                    }
                    break;

                case 'micro':
                    $versions = array(
                        'micro',
                        '2018'
                    );
                    foreach ($versions as $version) {
                        $body['procedure']['version'] = $version;

                        $url_search = $url . $type;

                        $line = \Consumer::consume($url_search, json_encode($body));

                        if ($line->errCode == 0) {
                            $group_lines = (array)$line;
                            if ($version == 'micro') {
                                $group_lines += ['type' => $type, 'version' => 'No'];
                            } else {
                                $group_lines += ['type' => $type, 'version' => $version];
                            }
                        }
                    }
                    break;

                case 'carga':
                    $versions = array(
                        'carga',
                        '2018'
                    );
                    foreach ($versions as $version) {
                        $body['procedure']['version'] = $version;
                        $url_search = $url . $type;

                        $line = \Consumer::consume($url_search, json_encode($body));

                        if ($line->errCode == 0) {
                            $group_lines = (array)$line;
                            if ($version == 'carga') {
                                $group_lines += ['type' => $type, 'version' => 'No'];
                            } else {
                                $group_lines += ['type' => $type, 'version' => $version];
                            }
                        }
                    }
                    break;

                default:
                    $url_search = $url . $type;

                    $line = \Consumer::consume($url_search, json_encode($body));

                    if ($line->errCode == 0) {
                        $group_lines = (array)$line;
                        $group_lines += ['type' => $type, 'version' => 'No'];
                    }
                    break;
            }
        }

        return $group_lines;
    }
}