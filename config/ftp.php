<?php
return array(

    /*
	|--------------------------------------------------------------------------
	| Default FTP Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the FTP connections below you wish
	| to use as your default connection for all ftp work.
	|
	*/

    'default' => 'FTP_licencia',

    /*
    |--------------------------------------------------------------------------
    | FTP Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the FTP connections setup for your application.
    |
    */

    'connections' => array(

        'FTP_licencia' => array(
            'host'   => '128.222.200.24',
            'port'  => 21,
            'username' => 'Administrador',
            'password'   => 'l1c3nc145@s3c',
            'passive'   => true,
        ),
        'FTP_ñora' => array(
            'host'   => '10.5.1.23',
            'port'  => 21,
            'username' => 'caca',
            'password'   => '123456',
            'passive'   => true,
        ),

    ),
);