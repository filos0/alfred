$(function () {
    $(".switch").bootstrapSwitch();

    $('.select').select2({
        minimumResultsForSearch: Infinity
    });
});