$(function () {
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        columnDefs: [{
            orderable: false,
            width: '100px'
        }],
        dom: '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filtro:</span> _INPUT_',
            lengthMenu: '<span>Mostrar:</span> _MENU_',
            paginate: {
                'first': 'Primero',
                'last': 'Ultimo',
                'next': '&rarr;',
                'previous': '&larr;'
            },
            info: 'Mostrando de _START_ a _END_ de _TOTAL_'
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }
    });

    $('.datatable-basic').DataTable();

    $('.datatable-module').DataTable({
        columnDefs: [
            { 'targets': 1 }
        ]
    });

    $('.datatable-roles').DataTable({
        columnDefs: [
            { 'targets': 1 }
        ],
        scrollCollapse: true,
        scrollY: '550px',
        paging: false,
    });

    $('.dataTables_filter input[type=search]').attr('placeholder','Escriba para Filtrar');

    $('.dataTables_length select').select2({
        minimumResultsForSearch: Infinity,
        width: 'auto'
    });
});