$(document).ready(function () {

    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });
    if (top.location.pathname === '/home' || top.location.pathname === '/') {
        setInterval(render_servicios, 20000);
        setInterval(render_home, 3000);
    }

    var Max = moment().format('YYYY-MM-DD');
    var Min = moment().format('2000-01-01');

    $('input[type ="date"]').each(function () {
        $(this).prop("max", Max).prop("min", Min);
    });

    $(':input').not('input.minusculas').addClass('text-uppercase'); //Mayusculas en todos los input

    $(':input').focus(function () {
        $(this).css("box-shadow", "0 0 0 500px rgba(0, 187, 236, 0.4) inset");
    }); // Tono azul en los input en focus
    $(':input').blur(function () {
        $(this).css("box-shadow", "none");
    });  // se restaura el tono cuando no se enfoque


    $('input[type = date]').keyup('change', function () {

        var fecha = $(this).val();

        var res_fecha = validar_Fechas(fecha);

        if (res_fecha == true) {
            $(this).css("background-color", "#FFFFFF");// normal
        }

        else if (res_fecha == 'futuro') {
            $(this).val('');
            $(this).focus();
            $(this).css("background-color", "#FFFFFF");// normal
            swal({
                title: "",
                text: "No puedes seleccionar una fecha posterior de hoy.",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $(this).focus();
            });

        }
        else {

            $(this).css("background-color", "#FF0000"); //rojo

        }

    });  // Valida Fechas


    $('.solo_numero').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = soloNumeros(valActual);
        $(this).val(nuevoValor);
    });
    $('.alfa_Numerico').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = alfaNumerico(valActual);
        $(this).val(nuevoValor);
    });
    $('.alfaNumerico_espacio').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = alfaNumerico_espacio(valActual);
        $(this).val(nuevoValor);
    });
    $('.solo_letra').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = soloLetras(valActual);
        $(this).val(nuevoValor);
    });
    $('.soloLetras_espacio').keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = soloLetras_espacio(valActual);
        $(this).val(nuevoValor);
    });


});

function soloLetras_espacio(nombre) {


    nombre = nombre.toString();
    nombre = nombre.match(/[.,a-zA-ZáÁéÉíÍóÓúÚñÑ|\s]+/);


    return nombre;
}
function soloLetras(nombre) {


    nombre = nombre.toString();
    nombre = nombre.match(/[,.a-zA-ZáÁéÉíÍóÓúÚñÑ]+/);


    return nombre;
}

function soloNumeros(numero) {

    console.log = function () {
    };
    numero = numero.toString();
    numero = numero.match(/[0-9]+/);

    return numero;


}

function alfaNumerico(cadena) {

    console.log = function () {
    };
    cadena = cadena.toString();
    cadena = cadena.match(/[0-9a-zA-ZáÁéÉíÍóÓúÚñÑ]+/);

    return cadena;


}
function alfaNumerico_espacio(cadena) {

    console.log = function () {
    };
    cadena = cadena.toString();
    cadena = cadena.match(/[.,0-9a-zA-ZáÁéÉíÍóÓúÚñÑ|\s]+/);

    return cadena;


}

function validar_Fechas(fecha) {

    var rxDatePattern = /^([1-2][0-9]{3})(\-)(\d{2})(\-)(\d{2})$/;

    var fecha_comp = fecha.match(rxDatePattern);

    if (fecha_comp == null)
        return false;

    var dtArray = fecha.split('-');

    Mes = dtArray[1];
    Dia = dtArray[2];
    Ano = dtArray[0];


    var fecha_hoy = moment();
    var fecha_asig = moment(fecha);


    if (Ano < 1900)
        return false;
    if (Mes < 1 || Mes > 12)
        return false;
    else if (Dia < 1 || Dia > 31)
        return false;
    else if ((Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11) && Dia == 31)
        return false;
    else if (Mes == 2) {
        var isleap = (Ano % 4 == 0 && (Ano % 100 != 0 || Ano % 400 == 0));
        if (Dia > 29 || (Dia == 29 && !isleap))
            return false;
    }

    if (fecha_asig.diff(fecha_hoy) >= 0) {
        return 'futuro';
    }

    return true;


}