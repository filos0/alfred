new Vue({
    el: '#app',
    data: {
        plate: '',
        seen: false,
        tipo: '',
        radio: ''
    },
    methods: {
        CheckPlate: function () {
            this.plate = this.plate.toUpperCase();
            var plate = this.plate.split("");
            var num = 0;
            var alfa = 0;

            for (var i = 0; i < this.plate.length; i++) {
                if (/[0-9]/.test(plate[i]) == true) {
                    num++;
                }

                if (/[A-Z]/.test(plate[i]) == true) {
                    alfa++;
                }
            }

            if ((alfa == 0 && num == 7) || (alfa == 3 && num == 6) || (alfa == 0 && num == 5)) {
                this.radio = 'MICRO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else {
                this.tipo = 'NINGUNO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            }
        }
    }
});