new Vue({
    el: '#app',
    data: {
        plate: '',
        seen: false,
        tipo: '',
        radio: ''
    },
    methods: {
        CheckPlate: function () {
            this.plate = this.plate.toUpperCase();
            var plate = this.plate.split("");
            var num = 0;
            var alfa = 0;

            for (var i = 0; i < this.plate.length; i++) {
                if (/[0-9]/.test(plate[i]) == true) {
                    num++;
                }

                if (/[A-Z]/.test(plate[i]) == true) {
                    alfa++;
                }
            }

            if ((alfa == 3 && num == 3) || (alfa == 4 && num == 2)) {
                this.radio = 'PARTICULAR';
                setTimeout(function () {
                    document.forms['post'].submit();
                }, 500);
            } else if ((alfa == 1 && num == 5) || (alfa == 0 && num == 6) || (alfa == 2 && num == 4)) {
                this.radio = 'TAXI';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else if ((alfa == 2 && num == 3) || (alfa == 3 && num == 2)) {
                this.tipo = ['ANTIGUO', 'MOTO','PARTICULAR'];
                this.seen = true;
            } else if ((alfa == 1 && num == 4) || (alfa == 0 && num == 5)) {
                this.tipo = 'MOTO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else {
                this.tipo = 'NINGUNO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            }
        }
    }
});