new Vue({
    el: '#app',
    data: {
        plate: '',
        seen: false,
        tipo: '',
        radio: ''
    },
    methods: {
        CheckPlate: function () {
            this.plate = this.plate.toUpperCase();
            var plate = this.plate.split("");
            var num = 0;
            var alfa = 0;

            for (var i = 0; i < this.plate.length; i++) {
                if (/[0-9]/.test(plate[i]) == true) {
                    num++;
                }

                if (/[A-Z]/.test(plate[i]) == true) {
                    alfa++;
                }
            }

            if ((alfa == 3 && num == 3) || (alfa == 4 && num == 2)) {
                this.radio = 'PARTICULAR';
                setTimeout(function () {
                    document.forms['post'].submit();
                }, 500);
            }else if(alfa == 2 && num == 4){
                this.tipo = ['CARGA', 'TAXI'];
                this.seen = true;
            } else if (alfa == 3 && num == 1) {
                this.radio = 'CARGA';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else if ((alfa == 0 && num == 7) || (alfa == 3 && num == 6)) {
                this.radio = 'MICRO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else if ((alfa == 1 && num == 5) || (alfa == 0 && num == 6)) {
                this.radio = 'TAXI';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else if ((alfa == 2 && num == 3) || (alfa == 3 && num == 2)) {
                this.tipo = ['ANTIGUO', 'MOTO','PARTICULAR'];
                this.seen = true;
            } else if (alfa == 1 && num == 4) {
                this.radio = 'MOTO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            } else if (alfa == 0 && num == 5) {
                this.tipo = ['MICRO', 'MOTO'];
                this.seen = true;
            } else {
                this.tipo = 'NINGUNO';
                setTimeout(function(){
                    document.forms['post'].submit();
                },500);
            }
        }
    }
});