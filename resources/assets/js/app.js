

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

var placa = new Vue({
    el: "#consulta_placa",
    data: {
        placa: "",
        marca: "",
        modelo: "",
        año: "",
        clase: "",
        niv: "",
        nci: "",
        ins: "",
        fec_ins: "",
        hor_ins: "",
        entidad: "",
        fec_emp: "",
        obs: "",
        roboS: false,
        roboN: false,
        fec_rob: "",
        fuen_rob: "",
        av: "",
        mp: "",
        fec_av: "",
        seen: false,
        error: false,
    },
    methods: {
        query: function () {
            if (this.placa){
                this.placa = this.placa.toUpperCase();

                axios.get(`repuve/consulta/placa/${this.placa}`).then(response => {
                    if(response.data[0].split(":")[0] === "ERR"){
                    this.error = true;
                    this.seen = false;
                }else{
                    this.placa = response.data[12];
                    this.marca = response.data[18];
                    this.modelo = response.data[16];
                    this.año = response.data[14];
                    this.clase = response.data[21];
                    this.niv = response.data[3];
                    this.nci = response.data[2];
                    this.ins = response.data[1];
                    this.fec_ins = response.data[4];
                    this.hor_ins = response.data[5];
                    this.entidad = response.data[17];
                    this.fec_emp = response.data[9];
                    this.obs = response.data[31];
                    this.seen = true;
                    this.error = false;
                }
            })
            .catch(error => {
                });

                axios.get(`repuve/consulta/robo/placa/${this.placa}`).then(response => {
                    if(response.data[0].split(":")[0] === "ERR"){
                    this.roboS = true;
                    this.roboN = false;
                }else{
                    this.fec_rob = response.data[1];
                    this.fuen_rob = response.data[4];
                    this.av = response.data[5];
                    this.mp = response.data[6];
                    this.fec_av = response.data[7];
                    this.roboS = false;
                    this.roboN = true;
                }
            }).catch(error => {});
            }else{
                this.error = true;
                this.seen = false;
            }
        }

    }
});

var niv = new Vue({
    el: "#consulta_niv",
    data: {
        placa: "",
        marca: "",
        modelo: "",
        año: "",
        clase: "",
        niv: "",
        nci: "",
        ins: "",
        fec_ins: "",
        hor_ins: "",
        entidad: "",
        fec_emp: "",
        obs: "",
        roboS: false,
        roboN: false,
        fec_rob: "",
        fuen_rob: "",
        av: "",
        mp: "",
        fec_av: "",
        seen: false,
        error: false,
    },
    methods: {
        query: function () {
            if (this.niv){
                this.niv = this.niv.toUpperCase();

                axios.get(`repuve/consulta/niv/${this.niv}`).then(response => {
                    if(response.data[0].split(":")[0] === "ERR"){
                    this.error = true;
                    this.seen = false;
                }else{
                    this.placa = response.data[12];
                    this.marca = response.data[18];
                    this.modelo = response.data[16];
                    this.año = response.data[14];
                    this.clase = response.data[21];
                    this.niv = response.data[3];
                    this.nci = response.data[2];
                    this.ins = response.data[1];
                    this.fec_ins = response.data[4];
                    this.hor_ins = response.data[5];
                    this.entidad = response.data[17];
                    this.fec_emp = response.data[9];
                    this.obs = response.data[31];
                    this.seen = true;
                    this.error = false;
                }
            })
            .catch(error => {
                });

                if(this.error){
                    this.roboS = false;
                    this.roboN = false;
                }else{
                    axios.get(`repuve/consulta/robo/niv/${this.niv}`).then(response => {
                        if(response.data[0].split(":")[0] === "ERR"){
                        this.roboS = true;
                        this.roboN = false;
                    }else{
                        this.fec_rob = response.data[1];
                        this.fuen_rob = response.data[4];
                        this.av = response.data[5];
                        this.mp = response.data[6];
                        this.fec_av = response.data[7];
                        this.roboS = false;
                        this.roboN = true;
                    }
                }).catch(error => {});
                }
            }else{
                this.error = true;
                this.seen = false;
            }
        }

    }
});

placa.placa = "";
placa.marca = "";
placa.modelo = "";
placa.año = "";
placa.clase = "";
placa.niv = "";
placa.nci = "";
placa.ins = "";
placa.fec_ins = "";
placa.hor_ins = "";
placa.entidad = "";
placa.fec_emp = "";
placa.obs = "";

niv.placa = "";
niv.marca = "";
niv.modelo = "";
niv.año = "";
niv.clase = "";
niv.niv = "";
niv.nci = "";
niv.ins = "";
niv.fec_ins = "";
niv.hor_ins = "";
niv.entidad = "";
niv.fec_emp = "";
niv.obs = "";