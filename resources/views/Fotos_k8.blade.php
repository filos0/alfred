@extends('layout.Master')

@push('assets')
    <link href="{{ asset( url('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') ) }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/sweetalert2.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/colors.css') }}" rel="stylesheet" type="text/css">


    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/uploaders/fileinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_advanced.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_sorting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('/js/pages/datatables_basic.js')}}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switch.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/blockui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/oklahomajs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/jquery.timer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/noty.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('content')
    <script type="text/javascript">

    </script>
    <div class="panel panel-flat">
        <div class="panel-heading text-center">
            <h3 class="panel-title"><b>Selecciona las Fotos para la Licencia</b></h3>
        </div>


        <form action="{{url('/K8/Actualizar/')}}" method="POST" id="form_curp" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="ultimo_folio" value="{{$folio}}">
            <input type="hidden" name="nuevo_folio">
            <input type="hidden" class="minusculas" name="url_foto" value="{{$foto}}">
            <input type="hidden" class="minusculas" name="url_firma" value="{{$firma}}">
            <input type="hidden" class="minusculas" name="url_huella2" value="{{$huella_i}}">
            <input type="hidden" class="minusculas" name="url_huella7" value="{{$huella_d}}">


            <div class="panel-body">
                <h4 class="content-group text-semibold">
                </h4>

                <div class="row">
                    @if(isset($ciudadano))
                        <div class="col-lg-4 col-md-6">
                            <br>
                            <br>
                            <br>
                            <br>
                            <h3 class="text-center"><b>DATOS:</b></h3>

                            <br>
                            <table class="table table-hover">
                                <tr>
                                    <td><b>Nombre:</b></td>
                                    <td>{{$ciudadano["materno"]}} {{$ciudadano["paterno"]}} {{$ciudadano["nombre"]}}
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>CURP/RFC</b></td>
                                    <td>{{$ciudadano["curp"]}}</td>
                                </tr>
                                <tr>
                                    <td><b>Sexo</b></td>
                                    <td>
                                        @if($ciudadano["sexo"] == 'M')
                                            MASCULINO
                                        @else
                                            FEMENINO
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Fecha de nacimiento</b></td>
                                    <td>{{$ciudadano["fecha_naci"]}}</td>
                                </tr>

                            </table>
                        </div>
                    @else
                        <div class="col-lg-3 col-md-6">
                        </div>
                    @endif

                    <div class="col-lg-3 col-md-6">

                        <div class="thumbnail no-padding">
                            <div class="thumb">
                                <img id="foto_img" src="{{asset($foto)}}" alt="">

                            </div>

                            <div class="caption text-center">
                                <h6 class="text-semibold no-margin"> Fotografía
                                    <small class="display-block"></small>
                                </h6>
                            </div>
                        </div>

                        <div class="col-md-7 col-md-offset-2">

                            <input type="button" class="btn bg-teal btn-block" value="CARGAR OTRA FOTO"
                                   onclick="document.getElementById('foto').click();"/>
                            <input type="file" style="display:none;" id="foto" name="otra_foto"/>

                        </div>

                    </div>

                    <div class="col-lg-3 col-md-6">

                        <div class="thumbnail no-padding">
                            <div class="thumb">
                                <img id="foto_firma" src="{{asset($firma)}}" alt="">
                            </div>

                            <div class="caption text-center">
                                <h6 class="text-semibold no-margin">Firma
                                    <small class="display-block"></small>
                                </h6>
                            </div>


                        </div>
                        <div class="col-md-7 col-md-offset-2">
                            <input type="button" class="btn bg-teal btn-block" value="CARGAR OTRA FOTO"
                                   onclick="document.getElementById('firma').click();"/>
                            <input type="file" style="display:none;" id="firma" name="otra_firma"/>
                        </div>

                    </div>

                    <div class="row col-md-12">
                        <br>
                        <br>
                        <br>
                        <div class="col-md-5 col-md-offset-3">
                            <button type="submit" class="btn bg-pink btn-block" id="saveimg"> Actualizar</button>
                        </div>

                    </div>

                </div>

            </div>
        </form>


    </div>


    <script type="text/javascript">
        var http = new XMLHttpRequest();
        var url = "{{asset($foto)}}"
        http.open('HEAD', url, false);
        http.send();
        var estatus_foto = http.status;
        url = "{{asset($firma)}}";
        http.open('HEAD', url, false);
        http.send();
        var estatus_firma = http.status;

        $('#form_curp').submit(function (e) {
            e.preventDefault();
            if ($('#firma').get(0).files.length == 0 || $('#foto').get(0).files.length == 0) { //No subieron fotos
                if (estatus_firma != 200 && estatus_foto != 200) { //Ni hay en servidor
                    swal({
                        title: 'Debes de Subir Foto y la Firma ',
                        type: 'error',
                        showCancelButton: false
                    }).then(function () {
                        return false;
                    });
                }
                else { // SI existen
                    swal({
                        title: 'Inserta el nuevo folio del trámite',
                        input: 'text',
                        showCancelButton: true,
                        inputValidator: function (value) {
                            return new Promise(function (resolve, reject) {
                                if (value) {
                                    resolve()
                                } else {
                                    reject('Necesitas escribir algo')
                                }
                            })
                        }
                    }).then(function (result) {
                        $("[name = nuevo_folio]").val(result);
                        $("#form_curp")[0].submit();
                    });
                }

            }
            else if ($('#firma').get(0).files.length != 0 && $('#foto').get(0).files.length != 0) { //subieron fotos
                swal({
                    title: 'Inserta el nuevo folio del trámite',
                    input: 'text',
                    showCancelButton: true,
                    inputValidator: function (value) {
                        return new Promise(function (resolve, reject) {
                            if (value) {
                                resolve()
                            } else {
                                reject('Necesitas escribir algo')
                            }
                        })
                    }
                }).then(function (result) {
                    $("[name = nuevo_folio]").val(result);
                    $("#form_curp")[0].submit();
                });
            }


        });

        function subir_foto(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#foto_img').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function subir_firma(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#foto_firma').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#firma").change(function () {
            subir_firma(this);
        });

        $("#foto").change(function () {
            subir_foto(this);
        });

    </script>

@endsection
