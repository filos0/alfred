@extends('layout.Master')

@push('assets')
    <link href="{{ asset( url('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') ) }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/sweetalert2.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/colors.css') }}" rel="stylesheet" type="text/css">


    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/uploaders/fileinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_advanced.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_sorting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('/js/pages/datatables_basic.js')}}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switch.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/blockui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/oklahomajs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/jquery.timer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/noty.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading text-center">
            <h3 class="panel-title"><b>Licencias.</b></h3>
        </div>

        <form action="{{url('/K8/Fotos/')}}" method="POST" id="form_curp">
            {{ csrf_field() }}
            <input type="hidden" name="curp_final">

            <div class="panel-body">

                <br>
                <div class="row ">
                    <div class="input-group content-group col-md-12">
                        <div class="col-md-4 col-md-offset-4">

                            <div class="text-center">
                                <h5><label>CURP/RFC</label></h5>
                            </div>
                            <input type="text" name="curp"
                                   class="form-control text-center text-uppercase alfa_Numerico" autofocus>

                        </div>


                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2 col-md-offset-5">

                        <button class="btn btn-xl bg-teal btn-block" type="button" onclick="buscar_curp()"><i
                                    class="icon icon-search4 text-white"></i> BUSCAR
                        </button>

                    </div>
                </div>
                <br>
                <hr>
                <br>
                <div class="row ">
                    <div class="input-group content-group col-md-12">

                        <div class="col-md-3 col-md-offset-1">

                            <div class="text-center">
                                <h5><label>Nombre(s)</label></h5>
                            </div>
                            <input type="text" name="nombre"
                                   class="form-control text-center text-uppercase soloLetras_espacio">

                        </div>
                        <div class="col-md-3 ">

                            <div class="text-center">
                                <h5><label>Primer Apellido</label></h5>
                            </div>
                            <input type="text" name="primer_apellido"
                                   class="form-control text-center text-uppercase soloLetras_espacio">

                        </div>
                        <div class="col-md-3 ">

                            <div class="text-center">
                                <h5><label>Segundo Apellido</label></h5>
                            </div>
                            <input type="text" name="segundo_apellido"
                                   class="form-control text-center text-uppercase soloLetras_espacio">

                        </div>


                    </div>

                </div>
                <br>
                <div class="row">
                    <div class="col-md-2 col-md-offset-5">

                        <button class="btn btn-xl bg-teal btn-block" type="button" onclick="buscar_nombre()"><i
                                    class="icon icon-search4 text-white"></i> BUSCAR
                        </button>

                    </div>
                </div>
                <hr>
                <br>
                <div class="row ">
                    <div class="input-group content-group col-md-12">
                        <div class="col-md-4 col-md-offset-4">

                            <div class="text-center">
                                <h5><label>Último Folio </label></h5>
                            </div>
                            <input type="text" name="folio"
                                   class="form-control text-center text-uppercase alfa_Numerico">

                        </div>


                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-2 col-md-offset-5">

                        <button class="btn btn-xl bg-teal btn-block" type="submit"><i
                                    class="icon icon-search4 text-white"></i> BUSCAR
                        </button>

                    </div>
                </div>


            </div>


    </div>


    <script type="text/javascript">
        function buscar_curp() {
            var curp = $("[name = curp]").val().toUpperCase();
            if (curp == "") {
                swal({
                    title: "Ingresa un CURP o RFC",
                    text: "",
                    type: "error",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $("[name = curp]").focus();
                    return;
                });
            }
            else {
                $.ajax({
                    url: "{{url('/API/K8_CURP/')}}",
                    type: "post",
                    data: {
                        "curp": curp
                    },
                    success: function (data) {
                        //console.log(data);
                        if (data.length == 0) {

                            swal({
                                title: "No hay registros",
                                text: "",
                                type: "error",
                                confirmButtonText: "Aceptar",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {
                                $("[name = curp]").focus();
                            });


                        }
                        else {

                            var html = "<br><br>" +
                                "<table id='tabla_propietario' class='table table-responsive'>" +
                                "<tr class='sin_hover'>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Clave</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Nombre</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Fecha Nacimiento</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Sexo</b></th>" +

                                "</tr>" +
                                "<tbody>";

                            $.each(data, function (i, propietario) {
                                var sexo;
                                if (propietario.sexo == 'M') {
                                    sexo = 'MASCULINO';
                                }
                                else if (propietario.sexo == 'F') {
                                    sexo = 'FEMENINO';
                                }
                                else {
                                    sexo = '-';
                                }
                                html = html + ('<tr onclick="aceptar_ciudadano(\'' + propietario.curp + '\')" >' +
                                    '<td >' + propietario.curp + '</td>' +
                                    '<td >' + propietario.paterno + ' ' + propietario.materno + ' ' + propietario.nombre + '</td>' +
                                    '<td >' + propietario.fecha_naci + '</td>' +
                                    '<td >' + sexo + '</td>' +

                                    '</tr>');

                            });

                            html = html + ("</tbody></table><br>");


                            swal({
                                title: "Selecciona el propietario que corresponde al ciudadano ",
                                html: html,
                                width: '800px',
                                type: "",
                                animation: false,
                                showCancelButton: false,
                                confirmButtonColor: "#FF0000",
                                confirmButtonText: "El ciudadano no se encuentra en la lista",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then(function () {

                                $("[name = curp]").val('').focus();
                            });


                        }
                    }

                });
            }
        }

        function buscar_nombre() {
            var nombre = $("[name = nombre]").val().toUpperCase();
            var primer_apellido = $("[name = primer_apellido]").val().toUpperCase();
            var segundo_apellido = $("[name = segundo_apellido]").val().toUpperCase();
            if (nombre == "" || primer_apellido == "" || segundo_apellido == "") {
                swal({
                    title: "Debes de llenar todos los campos",
                    text: "",
                    type: "error",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $("[name = nombre]").focus();
                    return;
                });
            }
            else {
                $.ajax({
                    url: "{{url('/API/K8_NOMBRE/')}}",
                    type: "post",
                    data: {
                        "nombre": nombre,
                        "primer_apellido": primer_apellido,
                        "segundo_apellido": segundo_apellido,
                    },
                    success: function (data) {
                        if (data.length == 0) {

                            swal({
                                title: "No hay registros",
                                text: "",
                                type: "error",
                                confirmButtonText: "Aceptar",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {
                                $("[name = curp]").focus();
                            });


                        }
                        else {

                            var html = "<br><br>" +
                                "<table id='tabla_propietario' class='table table-responsive'>" +
                                "<tr class='sin_hover'>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Clave</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Nombre</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Fecha Nacimiento</b></th>" +
                                "<th class='text-center' style='rgba(255,0,0,0.7);'><b>Sexo</b></th>" +

                                "</tr>" +
                                "<tbody>";

                            $.each(data, function (i, propietario) {
                                var sexo;
                                if (propietario.sexo == 'M') {
                                    sexo = 'MASCULINO';
                                }
                                else if (propietario.sexo == 'F') {
                                    sexo = 'FEMENINO';
                                }
                                else {
                                    sexo = '-';
                                }
                                html = html + ('<tr onclick="aceptar_ciudadano(\'' + propietario.curp + '\')" >' +
                                    '<td >' + propietario.curp + '</td>' +
                                    '<td >' + propietario.paterno + ' ' + propietario.materno + ' ' + propietario.nombre + '</td>' +
                                    '<td >' + propietario.fecha_naci + '</td>' +
                                    '<td >' + sexo + '</td>' +

                                    '</tr>');

                            });

                            html = html + ("</tbody></table><br>");


                            swal({
                                title: "Selecciona el propietario que corresponde al ciudadano ",
                                html: html,
                                width: '800px',
                                type: "",
                                animation: false,
                                showCancelButton: false,
                                confirmButtonColor: "#FF0000",
                                confirmButtonText: "El ciudadano no se encuentra en la lista",
                                allowOutsideClick: false,
                                allowEscapeKey: false
                            }).then(function () {

                                $("[name = primer_apellido]").val('');
                                $("[name = segundo_apellido]").val('');
                                $("[name = nombre]").val('').focus();
                            });


                        }
                    }

                });
            }
        }

        function aceptar_ciudadano(curp) {

            $('[name = curp_final]').val(curp);
            $('#form_curp').submit();
        }
    </script>

@endsection
