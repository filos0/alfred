@extends('layout.Master')

@push('assets')
    <link href="{{ asset( url('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') ) }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/sweetalert2.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/colors.css') }}" rel="stylesheet" type="text/css">


    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/uploaders/fileinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_advanced.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_sorting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('/js/pages/datatables_basic.js')}}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switch.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/blockui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/oklahomajs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/jquery.timer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/noty.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"><b>Comprobar Líneas De Captura</b></h5>
        </div>

        <div class="panel-body">
            <form action="{{url('/Linea_Captura/Comprobar')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row">
                    <div class="input-group content-group col-md-12">
                        <div class="col-md-5 col-md-offset-0">
                            <div class="col-md-offset-2">
                                <div class="text-center">
                                    <h5><label>Línea de Captura</label></h5>
                                </div>
                                <input type="text" name="linea_captura"
                                       class="form-control text-center input-xlg text-uppercase alfa_Numerico"
                                       maxlength="20" autofocus>
                            </div>
                        </div>

                        <div class="col-md-5 col-md-offset-0">

                            <div class=" col-md-offset-2">
                                <div class="text-center">
                                    <h5><label>Carga un archivo con Lineas de Captura (xls o xlsx)</label></h5>
                                    <h7><label>solo debe tener una hoja, elimine las demas</label></h7>
                                </div>

                                <input type="file" class="file-styled input-xlg" name="archivo_linea_captura">
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row col-md-10 ">
                    <div class="col-md-offset-5 col-md-4">
                        <br>
                        <button class="btn bg-pink btn-block" type="submit" onclick="espera()"> Comprobar <i
                                    class="fa fa-refresh text-white"></i></button>
                        <br>
                    </div>
                </div>
            </form>
            <div class="row ">
                <div class="row col-md-10  col-md-offset-1">
                    @if(isset($lineas))
                        <br>
                        <hr>
                        <br>

                        <div class="row text-center">
                            <h3>Resultado de Líneas de Captura</h3>
                            <br>
                            <a href="{{$ruta}}" target="_blank" class="btn bg-teal"> Descargar PDF</a>
                            <br>
                            <br>
                        </div>

                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="text-center" style="background: grey"><b>Línea</b></th>
                                <th class="text-center" style="background: grey"><b>Control</b></th>
                                <th class="text-center" style="background: grey"><b>Módulo</b></th>
                                <th class="text-center" style="background: grey"><b>Trámite</b></th>
                                <th class="text-center" style="background: grey"><b>Placa/Folio</b></th>
                                <th class="text-center" style="background: grey"><b>Fecha de Uso</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($lineas as $linea)

                                @if($linea['errCode'] == 1)
                                    <tr style="background: rgba(107,195,15,0.45)">
                                @elseif($linea['errCode'] == 3)
                                    <tr style="background: rgba(164,31,19,0.73)">
                                @else
                                    <tr>
                                        @endif
                                        <td class="text-center">{{$linea['linea']}}</td>
                                        <td class="text-center">{{$linea['cv']}}</td>
                                        <td class="text-center">{{$linea['modulo']}}</td>
                                        <td class="text-center">{{$linea['tramite']}}</td>
                                        <td class="text-center">{{$linea['placa']}}</td>
                                        <td class="text-center">{{$linea['fecha']}}</td>
                                    </tr>
                                    @endforeach
                            </tbody>
                        </table>
                    @endif
                </div>
            </div>

        </div>

    </div>
    <script type="text/javascript">
        function espera() {
            swal({
                title: "Espere un momento",
                text: "Este mensaje se cerrara automaticamente",
                type: "success",
                showCancelButton: false,
                confirmButtonColor: "#FF0000",
                confirmButtonText: "OK",
                allowOutsideClick: false,
                allowEscapeKey: false

            }).then(function () {
                espera();
            });

        }
    </script>

@endsection
