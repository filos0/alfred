<html>
<head>
    <html>
    <head>
        <style>
            .text-center {
                text-align: center;
            }

            body {
                font-family: sans-serif;
                font-size: 80%;

            }

            table {
                border-collapse: collapse;
            }

            table, th, td {
                border: 1px solid black;
            }

        </style>
    </head>
<body>

@php
    setlocale(LC_TIME,'spanish');
         $fecha = strftime("%A, %d de %B de %Y");
         $fecha = ucfirst(iconv("ISO-8859-1","UTF-8",$fecha));
         $fecha = mb_strtoupper($fecha,'utf-8');
        echo $fecha;
@endphp
<div id="content">
    <br><br>
    <div class="row text-center">
        <br>
        <h3>Resultado de Líneas de Captura</h3>
        <br>
    </div>
    <br><br>
    <br>
    <div class="text-center">
        <table>
            <thead>
            <tr>
                <th class="text-center"><b>Línea</b></th>
                <th class="text-center"><b>Control</b></th>
                <th class="text-center"><b>Módulo</b></th>
                <th class="text-center"><b>Trámite</b></th>
                <th class="text-center"><b>Placa/Folio</b></th>
                <th class="text-center"><b>Fecha de Uso</b></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $linea)

                @if($linea['errCode'] == 1)
                    <tr style="background: rgba(107,195,15,0.45)">
                @elseif($linea['errCode'] == 3)
                    <tr style="background: rgba(164,31,19,0.73)">
                @else
                    <tr>
                        @endif
                        <td class="text-center">{{$linea['linea']}}</td>
                        <td class="text-center">{{$linea['cv']}}</td>
                        <td class="text-center">{{$linea['modulo']}}</td>
                        <td class="text-center">{{$linea['tramite']}}</td>
                        <td class="text-center">{{$linea['placa']}}</td>
                        <td class="text-center">{{$linea['fecha']}}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>


    </div>

</div>
</body>

</html>