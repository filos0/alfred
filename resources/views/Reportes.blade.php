@extends('layout.Master')

@push('assets')
    <link href="{{ asset( url('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') ) }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/sweetalert2.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/colors.css') }}" rel="stylesheet" type="text/css">


    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/uploaders/fileinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_advanced.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_sorting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('/js/pages/datatables_basic.js')}}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switch.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/blockui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/oklahomajs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/jquery.timer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/noty.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title"></h5>
        </div>

        <div class="panel-body">
            <div class="text-center">
                <h4><label>Busqueda Por Fecha</label></h4>
            </div>
            <br>
            <br>
            <div class="input-group content-group col-md-12">
                <div class="row">
                    <form action="{{url('/Reportes/Generar')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-md-3 col-md-offset-4">
                            <input type="date" name="fecha"
                                   class="form-control text-center "
                                   autofocus required>
                        </div>
                        <div class="col-md-offset-0 col-md-2">

                            <button class="btn bg-pink btn-block" type="submit" onclick="espera()"><i
                                        class="icon icon-search4"></i> Buscar
                            </button>

                        </div>
                    </form>
                </div>
                <br>
                <br>
                <hr>
                <br>
                <div class="row ">
                    <div class="col-md-5 col-md-offset-1 ">
                        @if(isset($tramites))
                            @if($tramites != null)
                                <div class="row text-center">
                                    <h3>Trámites de Particular</h3>
                                </div>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center" style="background: grey"><b>Fecha</b></th>
                                        <th class="text-center" style="background: grey"><b>Tipo Trámite</b></th>
                                        <th class="text-center" style="background: grey"><b>Número de Trámites</b></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($tramites as $tramite)
                                        <tr>
                                            <td class="text-center">{{$tramite['FECHA']}}</td>
                                            <td class="text-center">{{$tramite['TRAMITE']}}</td>
                                            <td class="text-center">{{$tramite['TOTAL_TRAMITES']}}</td>

                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            @endif
                        @endif
                    </div>
                    <div class="col-md-5 ">
                        @if(isset($tramites_lic))
                            <div class="row text-center">
                                <h3>Trámites de Licencias</h3>
                            </div>
                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th class="text-center" style="background: grey"><b>Tipo de Licencia</b></th>
                                    <th class="text-center" style="background: grey"><b>Tipo de Movimiento</b></th>
                                    <th class="text-center" style="background: grey"><b>Total</b></th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($tramites_lic as $tramite)
                                    <tr>
                                        <td class="text-center">{{$tramite['tipo_licencia']}}</td>
                                        <td class="text-center">{{$tramite['tipo_movimineto']}}</td>
                                        <td class="text-center">{{$tramite['total']}}</td>

                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>

            </div>

        </div>
    </div>
    <script type="text/javascript">
        function espera() {
            if ($('[name = fecha]').val() != "") {
                swal({
                    title: "Espere un momento",
                    text: "Este mensaje se cerrara automaticamente",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#FF0000",
                    confirmButtonText: "OK",
                    allowOutsideClick: false,
                    allowEscapeKey: false

                }).then(function () {
                    espera();
                });
            }
            else {
                swal({
                    title: "Seleccione una fecha",
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#FF0000",
                    confirmButtonText: "OK",
                    allowOutsideClick: false,
                    allowEscapeKey: false

                }).then(function () {
                    $('[name = fecha]').focus();
                });
            }
        }
    </script>

@endsection
