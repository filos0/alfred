<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="shortcut icon" type="image/x-icon" href="img/semovi.ico"/>
    <title>Sabana de Licencias</title>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-9" style="font-size: 27px; font-weight: bold">
            SÁBANA DE CONSULTAS DE LICENCIA
        </div>
        <div class="col-xs-2">
            <img src="img/Logo-CDMX.png" class="img-responsive">
        </div>
    </div>
    <br><br>
    <div class="row" style="border: black 1px solid">
        <div class="col-xs-8">
            <div class="row" style="font-size: 15px">
                <div class="col-xs"
                     style="padding: 10px; border-bottom: black 1px solid; border-right: solid black 1px">
                    NOMBRE: {{ $name }}
                </div>

                <div class="col-xs"
                     style="padding: 10px; border-bottom: black 1px solid; border-right: solid black 1px">
                    CURP: {{ $curp }}
                </div>

                <div class="col-xs"
                     style="padding: 10px; border-bottom: black 1px solid; border-right: solid black 1px">
                    {{ $street }}
                    <br>
                    {{ $ext }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $int }}
                    <br>
                    {{ $suburb }}
                    <br>
                    {{ $mayoralty }}
                </div>

                <div class="col-xs"
                     style="padding: 10px; border-bottom: black 1px solid; border-right: solid black 1px">
                    TELEFONO: {{ $telephone }}
                </div>

                <div class="col-xs" style="padding: 10px; border-right: solid black 1px">
                    LICENCIA EXPEDIDA EN EL MODULO {{ $module }} EL {{ $date }} A LAS {{ $hour }}
                </div>
            </div>
        </div>
        <div class="col-xs-3">
            <img src="data:image/png;base64,{{ $photo }}" class="img-responsive">
        </div>
    </div>

    <div class="row">
        <div class="col-xs">
            <hr style="border-top: 1px solid black;">
        </div>
    </div>

    <div class="row" style="text-align: center; font-size: 12px; border: 1px solid black; font-weight: bold">
        <div class="col-xs" style="padding: 30px; padding-bottom: 10px; padding-top: 18px">
            INFORMACIÓN CONSULTADA: {{ $previous }}
        </div>
    </div>

    <div class="row">
        <div class="col-xs">
            <hr style="border-top: 1px solid black;">
        </div>
    </div>

    <div class="row" style="text-align: center; font-size: 12px; border: 1px solid black; font-weight: bold">
        <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 18px">FOLIO</div>

        <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 18px">AÑOS</div>

        <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 18px">EXPEDICIÓN</div>

        <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 18px">VIGENCIA</div>

        <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 18px">TRÁMITE</div>

        <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px">TIPO DE LICENCIA</div>
    </div>

    @foreach($registry as $regist)
        <div class="row" style="text-align: center; font-size: 12px; border: black solid 1px; border-top: white;">
            <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $regist->folio }}
            </div>

            <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $regist->valid }} años
            </div>

            <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $regist->date_begin }}
            </div>

            <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $regist->date_end }}
            </div>

            <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $regist->move }}
            </div>

            <div class="col-xs-1" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $regist->type_license }}
            </div>
        </div>
    @endforeach

    <div class="row">
        <div class="col-xs">
            <hr style="border-top: 1px solid black;">
        </div>
    </div>

    <div class="row" style="text-align: center; font-size: 12px; border: 1px solid black; font-weight: bold">
        <div class="col-md-12">
            CANDADOS
        </div>
    </div>

    <div class="row" style="text-align: center; font-size: 12px; border: 1px solid black; font-weight: bold">
        <div class="col-xs-3" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
            OFICIO
        </div>

        <div class="col-xs-3" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
            DESCRIPCIÓN
        </div>

        <div class="col-xs-3" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
            ESTATUS
        </div>
    </div>

    @foreach($padlocks as $padlock)
        <div class="row" style="text-align: center; font-size: 12px; border: black solid 1px; border-top: white;">
            <div class="col-xs-3" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $padlock->padlock_type }}
            </div>

            <div class="col-xs-3" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $padlock->description }}
            </div>

            <div class="col-xs-3" style="padding: 30px; padding-bottom: 10px; padding-top: 10px;">
                {{ $padlock->status }}
            </div>
        </div>
    @endforeach

    <br>

    <div class="row">
        <div class="col-xs-12" style="font-weight: bold; font-size: 8px; text-align: justify">
            "Con fundamento en el artículo 11 de la Ley de Protección de Datos Personales para el Distrito Federal, la
            información contenida en este
            documento no es oficial hasta que se confirme por escrito con la firma autógrafa del Servidor Público
            facultado, por lo que la información
            contenida en el mismo no es oficial de la Secretaria de Movilidad hasta que se encuentre debidamente firmado
            en original.Este
            documento es confidencial, dirigido para uso exclusivo del destinatario, quedando prohibida su distribución
            y/o difusión en cualquier
            modalidad sin previa autorización del Servidor Público que lo emite y coteja en los expedientes físicos del
            área que la detenta.”
        </div>
    </div>
</div>

<div style="position: fixed; left: 10px; bottom: 5%; width: 100%; font-size: 10px; font-weight: bold">
    <div class="row">
        <div class="col-xs-2 col-xs-offset-10">
            <img src="img/Logo_Dependencia.png" class="img-responsive">
        </div>
    </div>

    <div class="row">
        <div class="col-xs-2 col-xs-offset-10">
            Secretaria de Movilidad
        </div>
    </div>

    <div class="row">
        <div class="col-xs-2 col-xs-offset-10">
            Dirección de Sistemas
        </div>
    </div>

    <div class="row">
        <div class="col-xs-2">
            IMPRIMIO: {{ strtoupper(Auth::user()->username) }}
        </div>
        <div class="col-xs-3">
            EL DIA: {{ date('d/m/Y') }} A LAS: {{ date('H:i') }}
        </div>
        <div class="col-xs-2 col-xs-offset-4">
            www.semovi.cdmx.gob.mx
        </div>
    </div>
</div>
</body>
</html>
