@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('options')
    <div class="breadcrumb-line breadcrumb-line-component">
        <ul class="breadcrumb">
            <li><a href="{{ route('Sheet_License_Name') }}"><i class="icon-user"></i> Nombre</a></li>
            <li><a href="{{ route('Sheet_License_CURP') }}"><i class="icon-profile"></i> CURP</a></li>
            <li><a href="{{ route('Sheet_License_Folio') }}"><i class="icon-hash"></i> Folio</a></li>
        </ul>
    </div>
@endsection

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Licencias</h5>
        </div>

        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido Paterno</th>
                <th>Apellido Materno</th>
                <th>CURP</th>
                <th>Tipo</th>
                <th></th>
            </tr>
            </thead>
            <tbody>

            @foreach($licenses as $license)
                <tr>
                    <td>{{$license->name}}</td>
                    <td>{{$license->first_surname}}</td>
                    <td>{{$license->second_surname}}</td>
                    <td>{{$license->curp}}</td>
                    <td>{{$license->type}}</td>
                    <td>
                        <form action="{{ route('Sheet_License_Folio') }}" method="post" target="_blank">
                            {{ csrf_field() }}
                            <input hidden="hidden" name="folio" value="{{ $license->folio }}">
                            <input hidden="hidden" name="type" value="{{ $license->type }}">
                            <input hidden="hidden" name="search" value="{{ $search }}">
                            <button type="submit" class="btn btn-block btn-primary">Ver</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
