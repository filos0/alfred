@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <!-- Grid -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <!-- Vertical form -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Consulta por Folio</h5>
                </div>

                <div class="panel-body">
                    <form action="{{ route('sheet_license_folio') }}" method="post" target="_blank">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="folio">Folio</label>
                            <input type="text" class="form-control" name="folio" id="folio" required autofocus autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label for="type">Tipo de Licencia</label>
                            <select class="select" name="type" id="type">
                                @role('usr_lic_a')
                                <option value="A">Tipo A</option>
                                @endrole
                                @role('usr_lic_b')
                                <option value="B">Tipo B</option>
                                @endrole
                                @role('usr_lic_c')
                                <option value="C">Tipo C</option>
                                @endrole
                                @role('usr_lic_d')
                                <option value="D">Tipo D</option>
                                @endrole
                                @role('usr_lic_e')
                                <option value="E">Tipo E</option>
                                @endrole
                            </select>
                        </div>

                        <div class="form-group">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Buscar <i
                                            class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>

                        @if($errors->any())
                            <div class="form-group">
                                <div class="alert alert-danger text-center">
                                    <span style="font-weight: bold">
                                        No se han encontrado coincidencias<br>
                                        Verifique su busqueda
                                    </span>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
            </div>
            <!-- /vertical form -->
        </div>
    </div>
    <!-- /grid -->
@endsection