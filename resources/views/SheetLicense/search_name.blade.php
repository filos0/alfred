@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <!-- Grid -->
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <!-- Vertical form -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Consulta por Nombre</h5>
                </div>

                <div class="panel-body">
                    <form action="{{route('sheet_license_name')}}" method="post">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" name="name" required autofocus autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Apellido Paterno</label>
                            <input type="text" class="form-control" name="first_surname" required autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Apellido Materno</label>
                            <input type="text" class="form-control" name="second_surname" required autocomplete="off">
                        </div>

                        <div class="form-group">
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Buscar <i
                                            class="icon-arrow-right14 position-right"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /vertical form -->
        </div>
    </div>
    <!-- /grid -->
@endsection