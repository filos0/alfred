@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/resources/resource.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Vehiculos</h5>
        </div>

        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>Modelo</th>
                <th>Estatus</th>
                <th>Placa</th>
                <th>Tipo</th>
                <th>Serie Vehicular</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach ($results as $auto)
                <tr>
                    <td>{{$auto->model}}</td>
                    <td>{{$auto->status}}</td>
                    <td>{{$auto->plate}}</td>
                    <td>{{$auto->type}}</td>
                    <td>{{$auto->vin}}</td>
                    <td>
                        <form method="post" action="{{ route('sheet_vehicle_plate') }}" target="_blank">
                            {{ csrf_field() }}
                            <input hidden value="{{$auto->plate}}" name="plate">
                            <input hidden value="{{strtolower($auto->type)}}" name="type">
                            <button type="submit" class="btn btn-block btn-primary">Ver</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection