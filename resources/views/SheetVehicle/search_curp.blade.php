@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <!-- Vertical form -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Consulta por CURP / RFC</h5>
                </div>

                <div class="panel-body">
                    <form action="{{ route('sheet_vehicle_curp') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>CURP / RFC</label>
                            <input type="text" class="form-control" name="curp" autofocus>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Buscar <i
                                        class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        <!-- /vertical form -->
        </div>
    </div>
@endsection