@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <!-- Vertical form -->
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Consulta por Folio</h5>
                </div>

                <div class="panel-body">
                    <form action="{{ route('sheet_vehicle_folio') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Folio</label>
                            <input type="text" class="form-control" name="folio">
                        </div>

                        <div class="form-group">
                            <label>Control Vehicular</label>
                            <select class="select" name="type">
                                @role('usr_veh_particular')
                                <option value="particular">Particular</option>
                                @endrole

                                @role('usr_veh_taxi')
                                <option value="taxi">Taxi</option>
                                @endrole

                                @role('usr_veh_micro')
                                <option value="micro">Micro</option>
                                @endrole

                                @role('usr_veh_carga')
                                <option value="carga">Carga</option>
                                @endrole

                                @role('usr_veh_antiguo')
                                <option value="antiguo">Antiguo</option>
                                @endrole

                                @role('usr_veh_moto')
                                <option value="moto">Moto</option>
                                @endrole

                                @role('usr_veh_remolque')
                                <option value="remolque">Remolque</option>
                                @endrole

                                @role('usr_veh_escolta')
                                <option value="escolta">Escolta</option>
                                @endrole

                                @role('usr_veh_verde')
                                <option value="verde">Verde</option>
                                @endrole
                            </select>
                        </div>

                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Buscar <i
                                        class="icon-arrow-right14 position-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /vertical form -->
        </div>
    </div>
@endsection