<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" type="image/x-icon" href="img/semovi.ico"/>
    <title>SABANA {{$type}}</title>
</head>
<body style="font-family: 'Roboto', sans-serif">
<table style="width: 100%">
    <tr>
        <td rowspan="4" style="width: 14%"><img src="img/logo_cidudad.jpg" width="5000%"></td>

        <td colspan="5" style="font-size: 22px"><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GOBIERNO DE LA CIUDAD DE MÉXICO</span>
        </td>
    </tr>

    <tr>
        <td colspan="5" style="font-size: 14px; font-weight: bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            SECRETARÍA DE MOVILIDAD
        </td>
    </tr>

    <tr>
        <td style="font-size: 12px; height: 14px; font-weight: bold; text-decoration: underline; text-align: right">
            IMPRIMIO: {{strtoupper($user)}}</td>
        <td colspan="3"
            style="font-size: 12px; height: 14px; font-weight: bold; text-decoration: underline; text-align: right">
            FECHA: {{ date('d/m/Y') }}</td>
    </tr>

    <tr>
        <td colspan="5" style="font-size: 10px; text-decoration: underline"><br></td>
    </tr>

    <tr>
        <td colspan="6" style="text-align: center; font-size: 13px; height: 20px; font-weight: bold">REPORTE INFORMATIVO
            SOBRE VEHICULO DE SERVICIO {{$type}}</td>
    </tr>

    <tr>
        <td colspan="6">
            <hr style="border: solid 1px">
        </td>
    </tr>

    <tr>
        <td colspan="6" style="text-align: center; font-weight: bold">DATOS DEL PROPIETARIO</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Nombre / Razón:</td>
        <td colspan="2">{{$name}}</td>

        <td>RFC / CURP:</td>
        <td colspan="2">{{$curp}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Domicilio:</td>
        <td colspan="2">{{$address}}</td>

        <td>Colonia:</td>
        <td colspan="2">{{$suburb}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Delegación:</td>
        <td colspan="2">{{$mayoralty}}</td>

        <td>Sexo:</td>
        <td colspan="2">{{$sex}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Entidad:</td>
        <td colspan="2">{{$state}}</td>

        <td>CP:</td>
        <td colspan="2">{{$postal_code}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Telefono:</td>
        <td colspan="2">{{$telephone}}</td>
    </tr>

    <tr>
        <td colspan="6" style="text-align: center; font-weight: bold; height: 60px">DATOS DEL VEHICULO Y TARJETA DE
            CIRCULACIÓN
        </td>
    </tr>

    <tr>
        <td style="font-weight: bold">NIV:</td>
        <td colspan="2"
            style="border: solid black; width: 45%; font-weight: bold; text-align: center; font-size: 19px">{{$vin}}</td>

        <td style="font-weight: bold">Placas:</td>
        <td colspan="2" style="font-weight: bold; font-size: 19px">{{$plate}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Clase:</td>
        <td colspan="2">{{$class}}</td>

        <td>Marca:</td>
        <td colspan="2">{{$mark}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Modelo:</td>
        <td colspan="2">{{$model}}</td>

        <td>Linea:</td>
        <td colspan="2">{{$line}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>T. Vehiculo:</td>
        <td colspan="2">{{$type_vehicle}}</td>

        <td>Versión:</td>
        <td colspan="2">{{$version}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>T. Servicio</td>
        <td colspan="2">{{$type_service}}</td>

        <td>No. Motor</td>
        <td colspan="2">{{$engine_number}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Combustible:</td>
        <td colspan="2">{{$fuel}}</td>

        <td>Uso:</td>
        <td colspan="2">{{$use}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Poliza Seguro:</td>
        <td colspan="2">{{$policy}}</td>

        <td>REPUVE:</td>
        <td colspan="2">{{$repuve}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Cve. Vehicular:</td>
        <td colspan="2">{{$vehicular_key}}</td>

        <td>Fecha Alta:</td>
        <td colspan="2">{{$discharge_date}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Valor Factura:</td>
        <td colspan="2">{{$bill}}</td>

        <td>Operador Alta:</td>
        <td colspan="2">{{$usr_alta}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>No. Puertas:</td>
        <td colspan="2">{{$number_doors}}</td>

        <td>Placa Ant:</td>
        <td colspan="2">{{$previous_plate}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>No. Pasajeros:</td>
        <td colspan="2">{{$number_passenger}}</td>

        <td>Folio TC:</td>
        <td colspan="2">{{$folio_circulation_card}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>No. Cilindros:</td>
        <td colspan="2">{{$number_cylinders}}</td>

        <td>Estatus Vehiculo:</td>
        <td colspan="2">{{$status_vehicle}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Fecha del Ultimo Movimiento</td>
        <td colspan="2">{{ $date_movement  }}</td>

        <td>Estatus Tarjeta:</td>
        <td colspan="2">{{$status_card}}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Folio QR:</td>
        <td colspan="2">{{ $qr }}</td>

        <td>Ultimo Movimiento:</td>
        <td colspan="2">{{ $movement }}</td>
    </tr>

    <tr style="font-size: 14px">
        <td>Modulo del Ultimo Movimiento:</td>
        <td colspan="2">{{ $module }}</td>

        <td>Operador Ultimo Movimiento:</td>
        <td colspan="2">{{ $usr_mov }}</td>
    </tr>

    <tr>
        <td colspan="6" style="font-weight: bold; height: 30px">Observaciones</td>
    </tr>

    <tr>
        <td colspan="6" style="font-size: 12px; border: solid black; height: 80px;">
            @foreach($padlocks as $padlock)
                @if(empty($padlock->padlock_type))
                @else
                    TIPO DE CANDADO: {{$padlock->padlock_type}}<br>
                @endif

                @if(empty($padlock->motive))
                @else
                    DESCRIPCION: {{$padlock->motive}}<br><br>
                @endif
            @endforeach
        </td>
    </tr>

    <tr>
        <td colspan="6" style="font-weight: bold; font-size: 8px; height: 30px">"Con fundamento en el artículo 11 de la
            Ley de Protección de Datos Personales para el Distrito Federal, la información contenida en este
            documento no es oficial hasta que se confirme por escrito con la firma autógrafa del Servidor Público
            facultado, por lo que la información
            contenida en el mismo no es oficial de la Secretaria de Movilidad hasta que se encuentre debidamente firmado
            en original.Este
            documento es confidencial, dirigido para uso exclusivo del destinatario, quedando prohibida su distribución
            y/o difusión en cualquier
            modalidad sin previa autorización del Servidor Público que lo emite y coteja en los expedientes físicos del
            área que la detenta.”
        </td>
    </tr>
</table>
</body>
</html>