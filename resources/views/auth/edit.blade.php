@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <form action="{{ route('users_update') }}" method="post">
        {{ csrf_field() }}
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Editar Usuario</h5>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Datos Personales
                            </legend>

                            <div class="form-group">
                                <label>Nombre: </label>
                                <input type="text" class="form-control" name="name" placeholder="{{ $user->name }}"
                                       autocomplete="off" maxlength="100">
                            </div>

                            <div class="form-group">
                                <label>Primer Apellido: </label>
                                <input type="text" class="form-control" name="first_surname"
                                       placeholder="{{ $user->ap_paterno }}" autocomplete="off" maxlength="100">
                            </div>

                            <div class="form-group">
                                <label>Segundo Apellido: </label>
                                <input type="text" class="form-control" name="second_surname"
                                       placeholder="{{ $user->ap_materno }}" autocomplete="off" maxlength="100">
                            </div>

                            <div class="form-group">
                                <label>Usuario: </label>
                                <input type="text" class="form-control" name="username"
                                       placeholder="{{ $user->username }}" autocomplete="off" maxlength="100">
                            </div>

                            <input hidden="hidden" name="id" value="{{ encrypt($user->id) }}">

                            <div class="form-group">
                                <label>Modulo</label>
                                <select class="select" name="module">
                                    @foreach($modules as $module)
                                        @if($module->NumeroModulo == $user->id_modulo)
                                            <option value="{{ $module->NumeroModulo }}"
                                                    selected="selected">{{ $module->NombreModulo }}</option>
                                        @else
                                            <option value="{{ $module->NumeroModulo }}">{{ $module->NombreModulo }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Area a la que pertenece: </label>
                                <input type="text" class="form-control" name="belongs"
                                       placeholder="{{ $user->pertenece }}" autocomplete="off" maxlength="100">
                            </div>

                            <br>
                            <br>

                            <fieldset>
                                <legend class="text-orange-700 text-bold"><i class="icon-key position-left"></i> Si no
                                    desea cambiar la contraseña deje las casillas en blanco
                                </legend>

                                <div class="form-group">
                                    <label>Contraseña</label>
                                    <input type="password" class="form-control" name="password" autocomplete="off" maxlength="15">
                                </div>

                                <div class="form-group">
                                    <label>Repita la nueva contraseña</label>
                                    <input type="password" class="form-control" name="check_password"
                                           autocomplete="off" maxlength="15">
                                </div>
                            </fieldset>

                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-lock position-left"></i> Roles
                            </legend>

                            <table class="table datatable-roles table-hover">
                                <thead>
                                <tr>
                                    <th>Rol</th>
                                    <th class="text-center">Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->description }}</td>
                                        <td class="text-center">
                                            @if($user->hasRole($role->name))
                                                <div class="checkbox checkbox-switch">
                                                    <label>
                                                        <input type="checkbox" name="role[]" value="{{ $role->id }}"
                                                               data-off-color="danger" data-on-text="Activado"
                                                               data-off-text="Desactivado" class="switch"
                                                               checked="checked">
                                                    </label>
                                                </div>
                                            @else
                                                <div class="checkbox checkbox-switch">
                                                    <label>
                                                        <input type="checkbox" name="role[]" value="{{ $role->id }}"
                                                               data-off-color="danger" data-on-text="Activado"
                                                               data-off-text="Desactivado" class="switch">
                                                    </label>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Actualizar <i
                                class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
    </form>
@endsection