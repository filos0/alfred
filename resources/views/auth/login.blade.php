<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/semovi.ico"/>
    <title>Inicio de sesión | ALFRED</title>

    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->

</head>

<body class="login-container login-cover">


<div class="page-container">


    <div class="page-content">

        <div class="content-wrapper">
            <div class="content">
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                class="sr-only">Close</span>
                                    </button>
                                    <div class="text-left">
                                        <span class="text-semibold" style="font-size: larger">
                                            {{$error}}<br>
                                            Asegúrese de capturar su username sin incluir @.<br>
                                            Ejemplo: si antes ingresaba con <strong>abc</strong>@133.com ahora simplemente ingrese con <strong>abc</strong>. La contraseña se conserva
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
                <form class="form-horizontal" id="login" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="panel panel-body login-form">
                        <div class="text-center">

                            <h5 class="content-group cdmx-text"><img
                                        src="{{URL::asset('/img/logo_semovi_cdmx_498_75px.png')}}"
                                        alt="SEMOVI" width="250px"><br><br>ALFRED</h5>
                        </div>
                        <br>
                        <div class="form-group has-feedback has-feedback-left ">
                            <input id="username" type="text" class="form-control alfa_Numerico" name="username"
                                   placeholder="Usuario"
                                   autofocus autocomplete="false">
                            @if ($errors->has('email'))
                                <span class="text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif

                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left ">
                            <input id="password" type="password" class="form-control" name="password"
                                   placeholder="Contraseña">

                            @if ($errors->has('password'))
                                <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif

                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>

                    <!-- <div class="form-group login-options">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" class="styled" checked="checked">
                                        Recuerdame
                                    </label>
                                </div>

                                <div class="col-sm-6 text-right">
                                    <a href="{{ url('/password/reset') }}">¿Se te olvido la contraseña?</a>
                                </div>
                            </div>
                        </div>-->

                        <div class="form-group">
                            <br>

                            <button type="submit" class="btn bg-pink-400 btn-block">Entrar <i
                                        class="icon-circle-right2 position-right"></i></button>
                        </div>
                    <!--
                        <div class="content-divider text-muted form-group"><span>¿No tienes una cuenta ?</span></div>
                        <a href="{{ url('/register') }}" class="btn bg-slate btn-block content-group">Registrar</a>
-->
                    </div>
                </form>


            </div>


        </div>


    </div>

</div>


</body>


</html>
