@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Gestionar Usuarios</h5>
        </div>

        <table class="table datatable-basic table-hover">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Primer Apellido</th>
                <th>Segundo Apellido</th>
                <th>Estatus</th>
                <th>Modulo</th>
                <th>Usuario</th>
                <th>Area a la que pertenece</th>
                <th class="text-center">Opciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->first_surname }}</td>
                    <td>{{ $user->second_surname }}</td>
                    <td>{{ $user->status }}</td>
                    <td>{{ $user->module }}</td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->belongs }}</td>
                    <td class="text-center">
                        <ul class="icons-list">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-menu9"></i>
                                </a>

                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li><a href="{{ route('users_show', [ 'id' => $user->id]) }}"><i
                                                    class="icon-vcard"></i> Editar Usuario</a></li>
                                    <li class="divider"></li>

                                    @if($user->status == 'A')
                                        <li><a href="{{ route('users_disable', [ 'id' => $user->id ]) }}"
                                               class="text-danger"><i class=" icon-user-block"></i>
                                                Deshabilitar</a></li>
                                    @else
                                        <li><a href="{{ route('users_enable', [ 'id' => $user->id ]) }}"
                                               class="text-info"><i class=" icon-user-check"></i>
                                                Habilitar</a></li>
                                    @endif
                                </ul>
                            </li>
                        </ul>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection