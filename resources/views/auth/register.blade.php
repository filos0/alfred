@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <form action="{{ route('users_insert') }}" method="post">
        {{ csrf_field() }}
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Crear Usuario</h5>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-reading position-left"></i> Datos Personales
                            </legend>

                            <div class="form-group">
                                <label for="name">Nombre: </label>
                                <input type="text" class="form-control" name="name"
                                       autocomplete="off" required maxlength="100" id="name">
                            </div>

                            <div class="form-group">
                                <label for="first_surname">Primer Apellido: </label>
                                <input type="text" class="form-control" name="first_surname"
                                       autocomplete="off" required maxlength="100" id="first_surname">
                            </div>

                            <div class="form-group">
                                <label for="second_surname">Segundo Apellido: </label>
                                <input type="text" class="form-control" name="second_surname"
                                       autocomplete="off" maxlength="100" id="second_surname">
                            </div>

                            <div class="form-group">
                                <label for="username">Usuario: </label>
                                <input type="text" class="form-control" name="username"
                                       autocomplete="off" required maxlength="100" id="username">
                            </div>

                            <div class="form-group">
                                <label for="module">Modulo</label>
                                <select class="select" name="module" id="module">
                                    @foreach($modules as $module)
                                        <option value="{{ $module->NumeroModulo }}">{{ $module->NombreModulo }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="belongs">Area a la que pertenece: </label>
                                <input type="text" class="form-control" name="belongs"
                                       autocomplete="off" required maxlength="100" id="belongs">
                            </div>

                            <br>
                            <br>

                            <div class="form-group">
                                <label for="password">Contraseña</label>
                                <input type="password" class="form-control" name="password"
                                       autocomplete="off" required maxlength="15" id="password">
                            </div>

                            <div class="form-group">
                                <label for="check_password">Repita la nueva contraseña</label>
                                <input type="password" class="form-control" name="check_password"
                                       autocomplete="off" required maxlength="15" id="check_password">
                            </div>

                        </fieldset>
                    </div>

                    <div class="col-md-6">
                        <fieldset>
                            <legend class="text-semibold"><i class="icon-lock position-left"></i> Roles
                            </legend>

                            <table class="table datatable-roles table-hover">
                                <thead>
                                <tr>
                                    <th>Rol</th>
                                    <th class="text-center">Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($roles as $role)
                                    <tr>
                                        <td>{{ $role->description }}</td>
                                        <td class="text-center">
                                            <div class="checkbox checkbox-switch">
                                                <label>
                                                    <input type="checkbox" name="role[]" value="{{ $role->id }}"
                                                           data-off-color="danger" data-on-text="Activado"
                                                           data-off-text="Desactivado" class="switch">
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Crear <i
                                class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
    </form>
@endsection