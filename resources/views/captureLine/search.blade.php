@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')

    @if(session('success_'))
        <script type="text/javascript">
            $(document).ready(function () {
                $('#modal_success').modal('show')
            })
        </script>

        <div id="modal_success" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">{{ session('success_') }}</h5>
                    </div>

                    <div class="modal-body">
                        <h6><span class="text-semibold">Linea de Captura:</span> {{ session('capture') }}</h6>

                        <hr>

                        <h6><span class="text-semibold text-success">Pago</span></h6>

                        <div class="row">
                            <div class="col-md-4">
                                <h6><span class="text-semibold">Concepto:</span> {{ session('concept') }}</h6>
                            </div>

                            <div class="col-md-4 text-center">
                                <h6><span class="text-semibold">Placa:</span> {{ session('plate') }}</h6>
                            </div>

                            <div class="col-md-4 text-right">
                                <h6><span class="text-semibold">Pago:</span> ${{ session('cost') }}</h6>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h6><span class="text-semibold">Fecha de Pago:</span> {{ session('pay_date') }}</h6>
                            </div>

                            <div class="col-md-6 text-right">
                                <h6><span class="text-semibold">Motivo del Pago:</span> {{ session('description') }}
                                </h6>
                            </div>
                        </div>

                        <hr>

                        @if(session('procedure'))
                            <h6 class="text-semibold text-success">Uso</h6>

                            <div class="row text-center">
                                <div class="col-md-4">
                                    <h6><span class="text-semibold">{{ session('procedure') }}
                                            :</span> {{ strtoupper(session('use')) }}</h6>
                                </div>

                                <div class="col-md-4">
                                    <h6><span class="text-semibold">Revista:</span> {{ session('use_version') ?? 'No' }}
                                    </h6>
                                </div>

                                <div class="col-md-4">
                                    <h6><span class="text-semibold">Fecha de Uso:</span> {{ session('date_use') }}</h6>
                                </div>
                            </div>

                        @else
                            <h6 class="text-semibold text-warning">No hay uso</h6>
                        @endif
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link text-success" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if(session('error_'))
        <script type="text/javascript">
            $(document).ready(function () {
                $('#modal_error').modal('show')
            })
        </script>

        <div id="modal_error" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title">{{ session('error_') }}</h5>
                    </div>

                    <div class="modal-body">
                        <h6><span class="text-semibold">Linea de Captura:</span> {{ session('capture') }}</h6>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-link text-danger" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Verificar Lineas de Captura</h5>
                </div>

                <div class="panel-body">
                    <form method="post" action="{{ route('line_verify') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Ingrese la linea de captura</label>

                            <input class="form-control" autocomplete="off" maxlength="20" name="capture_line" required>
                        </div>

                        <div class="form-group text-right">
                            <button class="btn bg-primary" type="submit">
                                Buscar

                                <i class="icon-arrow-right14 position-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection