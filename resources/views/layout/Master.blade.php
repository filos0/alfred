<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="img/semovi.ico"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Alfred's </title>

    @stack('vue')

    @stack('assets')

</head>
<body>
<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a class="navbar-brand text-white" href="#">ALFRED</a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class=""></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav ">
            <li>
                <a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <b><a id="hora" class="navbar-brand"></a></b>
            </li>
        </ul>
    </div>
</div>

<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">
                <div class="navbar-header">
                    <ul class="nav navbar-nav visible-xs-block">
                        <li>
                            <a data-toggle="collapse" data-target="#navbar-mobile" class="legitRipple">
                                <i class="icon-tree5"></i>
                            </a>
                        </li>

                        <li>
                            <a class="sidebar-mobile-main-toggle legitRipple">
                                <i class="icon-paragraph-justify3"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a class="media-left"><img src="{{ asset('img/semovi.ico') }}"
                                                       class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">{{Auth::user()->name}}</span>
                                <div class="text-size-mini text-muted">
                                    {{Auth::user()->username}}
                                </div>
                            </div>
                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-cog3"></i>
                                        </a>

                                        <ul class="dropdown-menu dropdown-menu-left">
                                            <li>
                                                <a href="{{ route('user_password', ['user' => Auth::user()->username]) }}"><i
                                                            class="icon-key"></i>Cambiar Contraseña</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            @if(Session::has('free'))

                            @else
                                <li class="">
                                    <a href="{{ url('/') }}" class=" legitRipple">
                                        <i class="icon-home4"></i>

                                        <span>Inicio</span>
                                    </a>
                                </li>

                                @role(['usr_lin_cap'])
                                <li class="">
                                    <a href="{{url('/Linea_Captura/')}}" class="legitRipple">
                                        <i class="icon  icon-coins"></i>

                                        <span>Linea(s) de Captura</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_repo'])
                                <li class="">
                                    <a href="{{url('/Reportes/')}}" class="legitRipple">
                                        <i class="icon icon-file-excel"></i>

                                        <span>Reportes</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_k8'])
                                <li class="">
                                    <a href="{{url('/K8/')}}" class="legitRipple">
                                        <i class="icon  icon-vcard"></i>

                                        <span>K8</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_uni_trat'])
                                <li class="">
                                    <a href="{{url('/Unificacion/')}}" class="legitRipple">
                                        <i class="icon  icon-file-text2"></i>

                                        <span>Unificación de trámite</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_lic_a','usr_lic_b','usr_lic_c','usr_lic_d','usr_lic_e'])
                                <li class="">
                                    <a href="{{ route('Sheet_License_Name') }}" class="legitRipple">
                                        <i class="icon  icon-stack"></i>

                                        <span>Sabana de Licencias</span>
                                    </a>

                                    <ul class="navigation navigation-main navigation-accordion">
                                        <li>
                                            <a href="{{ route('Sheet_License_Name') }}">Por Nombre</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('Sheet_License_CURP') }}">Por CURP</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('Sheet_License_Folio') }}">Por Folio</a>
                                        </li>
                                    </ul>

                                </li>
                                @endrole

                                @role(['usr_veh_particular','usr_veh_carga','usr_veh_micro','usr_veh_antiguo','usr_veh_taxi','usr_veh_remolque','usr_veh_moto','usr_veh_escolta'])
                                <li>
                                    <a class="legitRipple">
                                        <i class="icon  icon-car"></i>

                                        <span>Sabana de Vehiculos</span>
                                    </a>

                                    <ul class="navigation navigation-main navigation-accordion">
                                        <li>
                                            <a href="{{ route('Sheet_Vehicle_Plate') }}">Por Placa</a>
                                        </li>

                                        <li>
                                            <a class="legitRipple">
                                                <span>Por Ciudadano</span>
                                            </a>

                                            <ul class="navigation navigation-main navigation-accordion">
                                                <li>
                                                    <a href="{{ route('Sheet_Vehicle_Name') }}">Por Nombre</a>
                                                </li>

                                                <li>
                                                    <a href="{{ route('Sheet_Vehicle_CURP') }}">Por CURP</a>
                                                </li>

                                                <li>
                                                    <a href="{{ route('Sheet_Vehicle_Business') }}">Por Razón Social</a>
                                                </li>
                                            </ul>
                                        </li>

                                        <li>
                                            <a href="{{ route('Sheet_Vehicle_VIN') }}">Por Número de Serie</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('Sheet_Vehicle_Folio') }}">Por Folio QR</a>
                                        </li>
                                    </ul>
                                </li>
                                @endrole

                                @role(['usr_rep'])
                                <li>
                                    <a class="legitRipple" href="{{ url('/repuve') }}">
                                        <i class="icon icon-lock"></i>
                                        <span>Consulta REPUVE Particular</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_ten'])
                                <li>
                                    <a class="legitRipple" href="{{ route('tenure') }}">
                                        <i class="icon icon-calendar2"></i>
                                        <span>Consulta Tenencia</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_placa_disp'])
                                <li>
                                    <a class="legitRipple" href="{{ route('available') }}">
                                        <i class="icon icon-stack4"></i>
                                        <span>Placas Disponibles</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_lc'])
                                <li>
                                    <a class="legitRipple" href="{{ route('line_search') }}">
                                        <i class="icon icon-barcode2"></i>
                                        <span>Verificar Linea de Captura</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_status_down'])
                                <li>
                                    <a class="legitRipple" href="{{ route('change_status') }}">
                                        <i class="icon icon-file-minus"></i>
                                        <span>Cambiar Estatus de Vehiculo</span>
                                    </a>
                                </li>
                                @endrole

                                @role(['usr_manage'])
                                <li class="">
                                    <a href="{{ route('Sheet_License_Name') }}" class="legitRipple">
                                        <i class="icon icon-user"></i>

                                        <span>Usuarios</span>
                                    </a>

                                    <ul class="navigation navigation-main navigation-accordion">
                                        <li>
                                            <a href="{{ route('users_manage') }}">Gestionar Usuarios</a>
                                        </li>

                                        <li>
                                            <a href="{{ route('users_create') }}">Crear Usuario</a>
                                        </li>
                                    </ul>

                                </li>
                                @endrole

                                <li>
                                    <a href="{{ url('/') }}" class="legitRipple"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="icon-switch2"></i>
                                        <span>Cerrar Sesión</span>
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            @endif
                        </ul>
                    </div>

                </div>
            </div>
        </div>

        <div class="content-wrapper">
            <div class="page-header">
                <div class="page-header-content"><br></div>
                @if(session('success'))
                    <script>
                        $(document).ready(function (e) {
                            new PNotify({
                                title: '¡ATENCION!',
                                text: '{{ session('success') }}',
                                addclass: 'bg-success'
                            });
                        })
                    </script>
                @endif

                @if(session('error'))
                    <script>
                        $(document).ready(function (e) {
                            new PNotify({
                                title: '¡ATENCION!',
                                text: '{{ session('error') }}',
                                addclass: 'bg-danger'
                            });
                        })
                    </script>
                @endif

                @yield('options')
            </div>

            <!-- Content area -->
            <div class="content">
                <script type="text/javascript">
                    moment.locale('es');

                    var fecha = moment().format('L') + " " + moment().format('LTS');

                    $('#hora').text(fecha);

                    function fecha_actual() {
                        var fecha = moment().format('L') + " " + moment().format('LTS');

                        $('#hora').text(fecha);
                    }

                    setInterval(fecha_actual, 1000);
                </script>

                @yield('content')

                <div class="footer text-muted">
                    <a href="http://www.semovi.cdmx.gob.mx/" target="_blank">SECRETARIA DE MOVILIDAD DE LA CIUDAD DE
                        MÉXICO</a> DIRECCIÓN DE SISTEMAS
                </div>

            </div>
        </div>
    </div>
</div>
@stack('scripts')
</body>
</html>
