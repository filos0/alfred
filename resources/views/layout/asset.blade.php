<!-- Global stylesheets -->
<link href="{{ asset( url('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') ) }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/core.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/components.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/colors.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/custom/colors.css') }}" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->