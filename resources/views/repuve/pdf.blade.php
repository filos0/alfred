<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <title> Consulta REPUVE</title>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3">
            <img src="img/segob.png" class="img-responsive">
        </div>

        <div class="col-xs-4" style="text-align: left; font-size: 22px; font-weight: bold">
            <span>&nbsp;&nbsp;&nbsp;&nbsp;REPORTE REPUVE</span>
        </div>

        <div class="col-xs-3">
            <img src="img/secretariado.png" class="img-responsive">
        </div>
    </div>

    <hr style="border-top: 3px solid #ff1e98; width: 100%">

    <div class="row">
        <div class="col-xs-2">

        </div>

        <div class="col-xs-6" style="text-align: center; font-size: 20px; font-weight: bold">
            <span>&nbsp;INFORMACIÓN DEL VEHÍCULO</span>
        </div>
    </div>

    <div class="row">
        <div class="col-xs">
            <table class="table table-condensed" style="text-align: center; vertical-align: middle;">
                <tr class="active">
                    <td>MARCA</td>

                    <td>{{$marca}}</td>
                </tr>

                <tr>
                    <td>MODELO</td>

                    <td>{{ $modelo }}</td>
                </tr>

                <tr>
                    <td>AÑO</td>

                    <td>{{ $año }}</td>
                </tr>

                <tr>
                    <td>CLASE</td>

                    <td>{{ $clase }}</td>
                </tr>

                <tr>
                    <td>
                        NÚMERO DE IDENTIFICACIÓN VEHICULAR (NIV)
                    </td>

                    <td style="text-align: center; vertical-align: middle">{{ $niv }}</td>
                </tr>

                <tr>
                    <td>
                        NÚMERO DE CONSTANCIA DE INSCRIPCIÓN (NCI)
                    </td>

                    <td style="text-align: center; vertical-align: middle">{{ $nci }}</td>
                </tr>

                <tr>
                    <td>PLACA</td>

                    <td>{{ $placa }}</td>
                </tr>

                <tr>
                    <td>VERSIÓN</td>

                    <td>{{ $version }}</td>
                </tr>

                <tr>
                    <td style="text-align: center; vertical-align: middle">INSTITUCIÓN QUE LO INSCRIBIÓ</td>

                    <td>{{ $instituto }}</td>
                </tr>

                <tr>
                    <td>FECHA DE INSCRIPCIÓN</td>

                    <td>{{ $fec_ins }}</td>
                </tr>

                <tr>
                    <td>HORA DE INSCRIPCIÓN</td>

                    <td>{{ $hor_ins }}</td>
                </tr>

                <tr>
                    <td>ENTIDAD QUE EMPLACÓ</td>

                    <td>{{ $entidad }}</td>
                </tr>

                <tr>
                    <td>FECHA DE EMPLACADO</td>

                    <td>{{ $fec_emp }}</td>
                </tr>

                <tr>
                    <td>OBSERVACIONES</td>

                    <td>{{ strtoupper($obs) }}</td>
                </tr>

                @if($robo == 1)
                    <tr class="danger">
                        <td colspan="2" style="font-weight: bold; font-size: 15px">CON REPORTE DE ROBO</td>
                    </tr>

                    <tr class="danger" style="font-weight: bold; font-size: 15px">
                        <td>FECHA DE ROBO</td>

                        <td>{{ $fec_rob }}</td>
                    </tr>

                    <tr class="danger" style="font-weight: bold; font-size: 15px">
                        <td>FUENTE DEL ROBO</td>

                        <td>{{ $fuen_rob }}</td>
                    </tr>

                    <tr class="danger" style="font-weight: bold; font-size: 15px">
                        <td>AVERIGUACIÓN</td>

                        <td>{{ $av }}</td>
                    </tr>

                    <tr class="danger" style="font-weight: bold; font-size: 15px">
                        <td>AGENTE MP</td>

                        <td>{{ $mp }}</td>
                    </tr>

                    <tr class="danger" style="font-weight: bold; font-size: 15px">
                        <td>FECHA DE AVERIGUACIÓN</td>

                        <td>{{ $fec_av }}</td>
                    </tr>
                @else
                    <tr class="info">
                        <td colspan="2" style="font-weight: bold; font-size: 15px">NO HAY REPORTE DE ROBO</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
</div>
<div style="position: fixed; left: 3%; bottom: 5%; width: 100%">
    <div class="col-xs-3" style="font-size: 11px">
        IMPRIMIO: {{ strtoupper(Auth::user()->name) }}
    </div>

    <div class="col-xs-offset-5 col-xs-3" style="font-size: 11px">
        EL DIA: {{ date('d/m/Y') }} A LAS: {{ date('H:i') }}
    </div>
</div>
<div style="position: fixed; left: 0; bottom: 0; width: 100%">
    <div class="col-xs-2">
        <img src="img/Logo-CDMX.png" class="img-responsive">
    </div>

    <div class="col-xs-6">

    </div>

    <div class="col-xs-2">
        <img src="img/Logo_Dependencia.png" class="img-responsive">
    </div>
</div>
</body>

</html>