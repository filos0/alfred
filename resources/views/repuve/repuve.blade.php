@extends('layout.Master')

@section('title','REPUVE | Inicio')

@push('vue')
    <script type="text/javascript" src="js/vue/vue.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript" src="js/repuve/repuve.js"></script>
@endpush

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/resources/resource.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')
    <div class="col-md-offset-3 col-md-6">
        <div class="panel panel-body">
            <div class="text-center">
                <div class="icon-object border-slate-300 text-slate-300"><i class="icon-steering-wheel"></i></div>
                <h5 class="content-group">Consulta REPUVE
                    <small class="display-block">Seleccione el medio de consulta</small>
                </h5>
            </div>

            @if($errors->has('placa'))
                <div class="alert alert-danger">
                    <center><strong>{{  $errors->first('placa') }}</strong></center>
                </div>
            @endif
            <div class="panel-group content-group-lg" id="accordion1">
                <div class="panel panel-white" id="consulta_placa">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#accordion-group1" v-on:click="close">Por Placa</a>
                        </h6>
                    </div>

                    <div id="accordion-group1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" placeholder="Placa" name="placa" v-model="placa"
                                       v-on:keyup.enter="query">
                                <div class="form-control-feedback">
                                    <i class="icon-car text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn bg-pink-400 btn-block"
                                        v-on:click="query">
                                    Consultar
                                    <i class="icon-circle-right2 position-right"></i>
                                </button>
                            </div>
                            <div v-if="error" class="alert alert-warning" style="text-align: center">
                                <strong>No se han encontrado datos</strong>
                            </div>
                            <div v-if="seen"
                                 style="font-family: Raleway, sans-serif; font-weight: 500; text-align: center"
                                 class="bg-pink-300">
                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        MARCA
                                    </div>

                                    <div class="col-md-6">
                                        @{{ marca }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        MODELO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ modelo }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        AÑO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ año }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        CLASE
                                    </div>

                                    <div class="col-md-6">
                                        @{{ clase }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        NÚMERO DE IDENTIFICACIÓN VEHICULAR (NIV)
                                    </div>

                                    <div class="col-md-6">
                                        @{{ niv }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        NÚMERO DE CONSTANCIA DE INSCRIPCIÓN (NCI)
                                    </div>

                                    <div class="col-md-6">
                                        @{{ nci }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        PLACA
                                    </div>

                                    <div class="col-md-6">
                                        @{{ placa }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        VERSIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ modelo }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        INSTITUCIÓN QUE LO INSCRIBIÓ
                                    </div>

                                    <div class="col-md-6">
                                        @{{ ins }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DE INSCRIPCIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_ins }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        HORA DE INSCRIPCIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ hor_ins }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        ENTIDAD QUE EMPLACÓ
                                    </div>

                                    <div class="col-md-6">
                                        @{{ entidad }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DE EMPLACADO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_emp }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        OBSERVACIONES
                                    </div>

                                    <div class="col-md-6">
                                        @{{ obs }}
                                    </div>
                                </div>
                            </div>
                            <div v-if="roboS"
                                 style="font-family: Raleway, sans-serif; font-weight: 500; text-align: center"
                                 class="bg-blue">
                                <div class="row well-sm">
                                    <div class="col-md-12">
                                        NO HAY REPORTE DE ROBO
                                    </div>
                                </div>
                            </div>
                            <div v-if="roboN"
                                 style="font-family: Raleway, sans-serif; font-weight: 500; text-align: center"
                                 class="bg-danger-300">
                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DEL ROBO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_rob }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FUENTE DEL ROBO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fuen_rob }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        AVERIGUACIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ av }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        AGENTE MP
                                    </div>

                                    <div class="col-md-6">
                                        @{{ mp }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DE AVERIGUACIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_av }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-white" id="consulta_niv">
                    <div class="panel-heading">
                        <h6 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion1"
                               href="#accordion-group2" v-on:click="close">Por NIV</a>
                        </h6>
                    </div>
                    <div id="accordion-group2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="text" class="form-control" placeholder="NIV" name="niv" v-model="niv"
                                       v-on:keyup.enter="query">
                                <div class="form-control-feedback">
                                    <i class="icon-barcode2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="button" class="btn bg-pink-400 btn-block" v-on:click="query">Consultar <i
                                            class="icon-circle-right2 position-right"></i></button>
                            </div>
                            <div v-if="error" class="alert alert-warning" style="text-align: center">
                                <strong>No se han encontrado datos</strong>
                            </div>
                            <div v-if="seen"
                                 style="font-family: Raleway, sans-serif; font-weight: 500; text-align: center"
                                 class="bg-pink-300">
                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        MARCA
                                    </div>

                                    <div class="col-md-6">
                                        @{{ marca }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        MODELO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ modelo }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        AÑO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ año }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        CLASE
                                    </div>

                                    <div class="col-md-6">
                                        @{{ clase }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        NÚMERO DE IDENTIFICACIÓN VEHICULAR (NIV)
                                    </div>

                                    <div class="col-md-6">
                                        @{{ niv }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        NÚMERO DE CONSTANCIA DE INSCRIPCIÓN (NCI)
                                    </div>

                                    <div class="col-md-6">
                                        @{{ nci }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        PLACA
                                    </div>

                                    <div class="col-md-6">
                                        @{{ placa }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        VERSIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ modelo }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        INSTITUCIÓN QUE LO INSCRIBIÓ
                                    </div>

                                    <div class="col-md-6">
                                        @{{ ins }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DE INSCRIPCIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_ins }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        HORA DE INSCRIPCIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ hor_ins }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        ENTIDAD QUE EMPLACÓ
                                    </div>

                                    <div class="col-md-6">
                                        @{{ entidad }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DE EMPLACADO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_emp }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        OBSERVACIONES
                                    </div>

                                    <div class="col-md-6">
                                        @{{ obs }}
                                    </div>
                                </div>
                            </div>
                            <div v-if="roboS"
                                 style="font-family: Raleway, sans-serif; font-weight: 500; text-align: center"
                                 class="bg-blue">
                                <div class="row well-sm">
                                    <div class="col-md-12">
                                        NO HAY REPORTE DE ROBO
                                    </div>
                                </div>
                            </div>
                            <div v-if="roboN"
                                 style="font-family: Raleway, sans-serif; font-weight: 500; text-align: center"
                                 class="bg-danger-300">
                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DEL ROBO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_rob }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FUENTE DEL ROBO
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fuen_rob }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        AVERIGUACIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ av }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        AGENTE MP
                                    </div>

                                    <div class="col-md-6">
                                        @{{ mp }}
                                    </div>
                                </div>

                                <div class="row well-sm">
                                    <div class="col-md-6">
                                        FECHA DE AVERIGUACIÓN
                                    </div>

                                    <div class="col-md-6">
                                        @{{ fec_av }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
