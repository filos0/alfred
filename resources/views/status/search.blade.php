@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/notifications/pnotify.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Cambiar Estatus a Baja de Vehiculo Particular</h5>
                </div>

                <div class="panel-body">
                    <form method="post" action="{{ route('changed_status') }}">
                        {{ csrf_field() }}
                        <div class="form-group has-feedback has-feedback-left">
                            <label>Ingrese la placa</label>

                            <input class="form-control" autocomplete="off" maxlength="10" name="plate" required>

                            <div class="form-control-feedback">
                                <i class="icon-car"></i>
                            </div>
                        </div>

                        <div class="form-group text-right">
                            <button class="btn bg-primary" type="submit">
                                Buscar

                                <i class="icon-arrow-right14 position-right"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection