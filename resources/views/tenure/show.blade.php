@extends('layout.Master')

@push('assets')
    @include('layout.asset')

    <!-- Core JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/uniform.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/core/libraries/jquery_ui/interactions.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets/js/core/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/auth/edit.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/pages/custom/components/datatables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/resources/resource.js') }}"></script>
    <!-- /theme JS files -->
@endpush

@section('content')

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Lista de Tenencias</h5>
                </div>

                <table class="table datatable-basic table-hover">
                    <thead>
                    <tr>
                        <th>Linea de Captura</th>
                        <th>Concepto</th>
                        <th>Periodo</th>
                        <th>Importe Pagado</th>
                        <th>Placa</th>
                    </tr>
                    <tbody>
                    @foreach($tenures as $tenure)
                        <tr>
                            <td>{{ $tenure->lc }}</td>
                            <td>{{ $tenure->idconcepto }}</td>
                            <td>{{ $tenure->perini }}</td>
                            <td>${{ $tenure->total }}</td>
                            <td>{{ $tenure->placa }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection