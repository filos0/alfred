@extends('layout.Master')

@push('assets')
    <link href="{{ asset( url('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900') ) }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/sweetalert2.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/core.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/components.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/colors.css') }}" rel="stylesheet" type="text/css">


    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/pace.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/uploader_bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/uploaders/fileinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/libraries/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_advanced.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/datatables_sorting.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('/js/pages/datatables_basic.js')}}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switchery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/switch.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/plugins/loaders/blockui.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/core/app.js') }}"></script>


    <script type="text/javascript" src="{{ asset('/js/plugins/forms/wizards/stepy.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/core/libraries/jasny-bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_inputs.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_bootstrap_select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/sweetalert2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/bootstrap_select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/ui/ripple.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/forms/styling/uniform.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/oklahomajs/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/oklahomajs/jquery.timer.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/noty.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/jgrowl.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/components_notifications_other.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/plugins/notifications/bootbox.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/pages/form_checkboxes_radios.js') }}"></script>
@endpush

@section('title', 'Unificar tramite')


@section('content')

    <!-- /theme JS files -->

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="panel panel-flat">
        <div class="panel-heading text-center">
            <h3 class="panel-title"><b>Unificar Tramites de Licencia</b></h3>
        </div>


        <div class="panel-body">

            <div class="panel-heading">
                <h5 class="panel-title">Para unificar tramites, se requiere validar que el CURP o RFC
                    existan</h5>
                <br>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>

                        <tr>
                            <td style="width: 20%;"><b>Validar CURP:</b></td>
                            <form action="{{url('Unificacion/buscar')}}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <td style="width: 20%;"><p><input autofocus type="text" maxlength="18"
                                                                  style="text-transform:uppercase;"
                                                                  onkeyup="javascript:this.value=this.value.toUpperCase();"
                                                                  name='curp' id='curp'
                                                                  class="form-control alfa_Numerico">
                                    </p></td>

                                <td>
                                    <button type="submit" id="Validar" class="btn btn-primary">Validar</button>
                                </td>
                            </form>
                        </tr>


                        </tbody>
                    </table>
                </div>


            </div>
        </div>
    </div>


@endsection
