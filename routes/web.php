<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/Linea_Captura', 'middleware' => ['role:usr_lin_cap', 'auth']], function () {
    Route::get('/', 'Linea_Captura_Controller@inicio');

    Route::post('Comprobar', 'Linea_Captura_Controller@comprobar');
});

Route::group(['prefix' => '/Reportes', 'middleware' => ['role:usr_repo', 'auth']], function () {
    Route::get('/', 'Reportes_Controller@inicio');

    Route::post('Generar', 'Reportes_Controller@generar_Reporte');
});

Route::any('/PDF/{numero}', 'PDF_Controller@bajar_pdf');

Route::group(['prefix' => '/Unificacion', 'middleware' => ['role:usr_uni_trat', 'auth']], function () {
    Route::get('/', 'UnificacionController@inicio');

    Route::post('buscar', 'UnificacionController@buscar');
});

Route::group(['prefix' => '/K8', 'middleware' => ['role:usr_k8', 'auth']], function () {
    Route::get('/', 'K8_Controller@inicio');

    Route::post('Fotos', 'K8_Controller@get_Fotos');

    Route::post('Actualizar', 'K8_Controller@actualizar');
});

Route::group(['prefix' => '/API', 'middleware' => ['role:usr_k8', 'auth']], function () {
    Route::post('K8_CURP', 'API_Controller@lista_curp');

    Route::post('K8_NOMBRE', 'API_Controller@lista_nombre');
});


Route::group(['prefix' => '/SabanaLicencias', 'middleware' => ['role:usr_lic_a|usr_lic_b|usr_lic_c|usr_lic_d|usr_lic_e', 'auth']], function () {
    Route::get('/', 'SheetLicense\RoutesController@Name')->name('Sheet_License_Name');

    Route::get('curp', 'SheetLicense\RoutesController@CURP')->name('Sheet_License_CURP');

    Route::get('folio', 'SheetLicense\RoutesController@Folio')->name('Sheet_License_Folio');

    Route::post('nombre', 'SheetLicense\ConsultController@ConsultName')->name('sheet_license_name');

    Route::post('curp', 'SheetLicense\ConsultController@ConsultCurp')->name('sheet_license_curp');

    Route::post('folio', 'SheetLicense\ConsultController@ConsultFolio')->name('sheet_license_folio');

    Route::get('pdf/{type}/{folio}', 'SheetLicense\ConsultController@MakePDF')->name('Sheet_License_PDF');
});


Route::prefix('/SabanaVehiculos')->middleware(['auth', 'role:usr_veh_particular|usr_veh_carga|usr_veh_micro|usr_veh_antiguo|usr_veh_taxi|usr_veh_remolque|usr_veh_moto|usr_veh_escolta'])->group(function () {
    Route::get('nombre', function () {
        return view('SheetVehicle.search_name');
    })->name('Sheet_Vehicle_Name');

    Route::get('razon', function () {
        return view('SheetVehicle.search_business_name');
    })->name('Sheet_Vehicle_Business');

    Route::get('curp', function () {
        return view('SheetVehicle.search_curp');
    })->name('Sheet_Vehicle_CURP');

    Route::get('placa', function () {
        return view('SheetVehicle.search_plate');
    })->name('Sheet_Vehicle_Plate');

    Route::get('serie', function () {
        return view('SheetVehicle.search_vin');
    })->name('Sheet_Vehicle_VIN');

    Route::get('folio', function () {
        return view('SheetVehicle.search_folio_card');
    })->name('Sheet_Vehicle_Folio');

    Route::post('nombre', 'SheetVehicle\ConsultController@ConsultName')->name('sheet_vehicle_name');

    Route::post('curp', 'SheetVehicle\ConsultController@ConsultCURP_RFC')->name('sheet_vehicle_curp');

    Route::post('razon', 'SheetVehicle\ConsultController@ConsultBusiness')->name('sheet_vehicle_business');

    Route::get('vehiculo/{placa}', 'SheetVehicle\ConsultController@ConsultVehicle')->name('sheet_vehicle');

    Route::post('placa', 'SheetVehicle\ConsultController@ConsultPlate')->name('sheet_vehicle_plate');

    Route::post('serie', 'SheetVehicle\ConsultController@ConsultVIN')->name('sheet_vehicle_vin');

    Route::post('folio', 'SheetVehicle\ConsultController@ConsultFolio')->name('sheet_vehicle_folio');
});

Route::prefix('/repuve')->middleware(['role:usr_rep', 'auth'])->group(function () {
    Route::get('/', 'Repuve\RouteController@repuve');

    Route::get('consulta/placa/{placa}', 'Repuve\ConsultaController@consultPlate');
    Route::get('consulta/robo/placa/{placa}', 'Repuve\ConsultaController@stolePlate');

    Route::get('consulta/niv/{niv}', 'Repuve\ConsultaController@consultVIN');
    Route::get('consulta/robo/niv/{niv}', 'Repuve\ConsultaController@stoleVIN');

    Route::get('pdf/{consulta}', 'Repuve\ConsultaController@pdf');
});

Route::prefix('/tenencia')->middleware(['auth', 'role:usr_ten'])->group(function () {

    Route::get('/', function () {
        return view('tenure.search');
    })->name('tenure');

    Route::post('/consultar', 'Tenure\TenureController@CheckTenure')->name('tenure_search');
});

Route::middleware(['auth', 'role:usr_placa_disp'])->group(function () {
    Route::get('placas_disponibles', 'AvailablePlate\ConsultController@GetAvailablePlates')->name('available');
});

Route::get('error', function () {
    return view('errors.general');
})->name('error');

Route::get('403', function () {
    return view('errors.403');
})->name('error-403');

Route::get('500', function () {
    return view('errors.1000');
})->name('error-500');


Route::prefix('/usuarios')->middleware(['auth', 'role:usr_manage'])->namespace('Auth')->group(function () {

    Route::get('ver', 'ManageController@GetUsers')->name('users_manage');

    Route::get('editar/{id}', 'ManageController@ShowUser')->name('users_show');

    Route::post('editar', 'ManageController@UpdateUser')->name('users_update');

    Route::get('desabilitar/{id}', 'ManageController@DisableUser')->name('users_disable');

    Route::get('habilitar/{id}', 'ManageController@EnableUser')->name('users_enable');

    Route::get('crear', 'RegisterController@CreateUser')->name('users_create');

    Route::post('crear', 'RegisterController@InsertUser')->name('users_insert');

});

Route::prefix('/{user}')->middleware(['auth'])->namespace('Auth')->group(function () {

    Route::get('editar/contraseña', function () {
        return view('auth.reset_password');
    })->name('user_password');

    Route::post('editar/contraseña', 'ResetPasswordController@Reset')->name('user_reset');

});

Route::middleware(['auth', 'role:usr_status_down'])->namespace('ChangeStatusVehicle')->group(function () {
    Route::get('cambiar_estatus', function () {
        return view('status.search');
    })->name('change_status');

    Route::post('cambiar_estatus', 'ChangeController@changeDown')->name('changed_status');
});

Route::middleware(['auth'])->namespace('CaptureLines')->group(function () {
    Route::get('linea_captura', function() {
        return view('captureLine.search');
    })->name('line_search');

    Route::post('linea_captura', 'CheckController@check')->name('line_verify');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
